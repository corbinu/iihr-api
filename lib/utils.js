var slug = require('slug');

var internal = {};
var utils = {
    internal: internal
};

internal.encodeCharx = function(original) {
    var hex= ['0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'];

    var found = true;
    var thecharchar = original.charAt(0);
    var thechar = original.charCodeAt(0);

    switch (thecharchar) {
        case '\n':
            return "\\n";
        case '\r':
            return "\\r";
        case '\'':
            return "\\'";
        case '"':
            return "\\\"";
        case '\\':
            return "\\\\";
        case '\t':
            return "\\t";
        case '\b':
            return "\\b";
        case '\f':
            return "\\f";
        case '/':
            return "\\x2F";
        case '<':
            return "\\x3C";
        case '>':
            return "\\x3E";
        default:
            found = false;
            break;
    }

    if ( ! found) {
        if (thechar > 127) {
            var c = thechar;
            var a4 = c%16;
            c = Math.floor(c/16);
            var a3 = c%16;
            c = Math.floor(c/16);
            var a2 = c%16;
            c = Math.floor(c/16);
            var a1 = c%16;
            return "\\u" + hex[a1] + hex[a2] + hex[a3] + hex[a4] + "";
        } else {
            return original;
        }
    }

};

utils.slug = function(text) {
	return slug(text.replace("@","-").replace(".","-").replace("/","-").replace("\\", "-").replace("_", "-").toLowerCase());
};

utils.objPrettyString = function(obj) {
	return JSON.stringify(obj, null, '\t');
};

utils.stringEncode = function(preescape) {
	var escaped = "";

	for (var i = 0; i < preescape.length; i++) {
		escaped = escaped + internal.encodeCharx(preescape.charAt(i));
	}

	return escaped;
};

module.exports = utils;
