var MRStore = require('mrstore');
var bcrypt = require('bcrypt');
var pbkdf2 = require('pbkdf2-sha256');
var async = require('async');

var utils = require('../../utils');

var install = [
    {
        "username": "demo",
        "firstname": "Demo",
        "lastname": "User",
        "email": "demo@example.com",
        "password": "password",
        "roles": [
            "registered",
            "manager"
        ]
    },
    {
        "username": "demo2",
        "firstname": "Demo2",
        "lastname": "User2",
        "email": "demo2@example.com",
        "password": "password2",
        "roles": [
            "registered",
            "manager"
        ]
    },
    {
        "username": "demo3",
        "firstname": "Demo3",
        "lastname": "User3",
        "email": "demo3@example.com",
        "password": "password3",
        "roles": [
            "registered",
            "manager"
        ]
    }
];

var User = new MRStore('User', {
    id: {
        _default: function(_this) {
            return utils.slug(_this.email).toLowerCase();
        }
    },
    username: {
        _type: String,
        _index: true,
        _unique: true,
        _required: true
    },
    firstname: String,
    lastname: String,
    key: {
        _type: String,
        _unique: true,
        _required: true,
        _default: function(_this) {
            return User.internal.gen_key(_this.password, User.internal.config.key_salt);
        }
    },
    email: {
        _type: String,
        _required: true
    },
    password: {
        _type: String,
        _required: true
    },
    roles: {
        _type: Array,
        _default: []
    },
    created: {
        _type: Date,
        _default: Date.now,
        _required: true
    }
});

User.internal.hash_pass = function (password, password_salt, callback) {
	bcrypt.hash(password_salt + password, 10, function(err, hash) {
		callback(err, hash);
	});
};

User.internal.gen_key = function (password, key_salt) {
	return pbkdf2(password, new Buffer(key_salt), 1, 64).toString('hex');
};

User.reset = function (callback) {

    User.removeAll(function (err) {
        if (err) return callback(err);

        User.add(JSON.parse(JSON.stringify(install)), callback);
    });

};

User.update = function (updates, callback) {
    var _this = this;

    var id = updates.id;
    delete updates.id;
    delete updates.key;
    delete updates.created;

    var doUpdate = function () {
        User.get(id, function(err, doc) {
            if (err) return callback(err);
            for (var key in updates) {
                if (updates.hasOwnProperty(key)) {
                    doc[key] = updates[key];
                }
            }
            User.upsert(doc, callback);
        });
    };

    if (updates.password) {
        _this.internal.hash_pass(updates.password, _this.internal.config.password_salt, function (err, hash) {
            if (err) return callback(err, null);
            updates.password = hash;
            updates.key = User.internal.gen_key(hash, _this.internal.config.key_salt);
            doUpdate();
        });
    }
    else {
        doUpdate();
    }
};

User.add = function (doc, callback) {
    var _this = this;

    var insert = function(doc, callback) {
        _this.internal.hash_pass(doc.password, _this.internal.config.password_salt, function (err, hash) {
            if (err) return callback(err, null);

            doc.password = hash;

            User.insert(doc, callback);
        });
    };

    if (Object.prototype.toString.call(doc) === '[object Array]') {
        var inserts = [];

        doc.forEach(function(element) {
            inserts.push(function(inserted) {
                insert(element, inserted);
            });
        });

        async.parallel(inserts, function(err, results) {
            callback(err, results);
        });
    } else {
        insert(doc, callback);
    }

};

User.validate = function (userPassword, submittedPassword, callback) {
    var _this = this;

    bcrypt.compare(_this.internal.config.password_salt + submittedPassword, userPassword, function (err, isValid) {

        callback(err, isValid);
    });
};

User.checkRole = function (roles, users, user) {
    var auth = roles.every(function (role) {
        return (user.roles.indexOf(role) !== -1);
    });

    if ( ! auth) {
        auth = (users.indexOf(user.id) !== -1);
    }

    return auth;
};

User.setup = function(config, db) {
    var _this = this;

    _this.internal.config = config;
    _this.internal.db = db;
};

User.start = function(callback) {
    var _this = this;
    User.connect(_this.internal.db.mrstore.users, function(err) {
        if (err) return callback(err);
        User.installViews(callback);
    });
};

module.exports = User;