var models = {};

models.resource =  require('./resource');
models.user =  require('./user');

models.setup = function(config, db) {
	models.resource.setup(config, db);
	models.user.setup(config, db);
};

models.start = function(callback) {
    models.resource.start(function(err) {
        if (err) return callback(err);

        models.user.start(callback);
    });
};

module.exports = models;