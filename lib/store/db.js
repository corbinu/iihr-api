var Datastore = require('nedb');
var elasticsearch = require('elasticsearch');
var couchbase = require('couchbase');

var mrstoreConnect = function(config) {
    var mrstore = {};

    switch (config.mrstore.type) {
        case "mock":
            mrstore.users = {
                type: 'nedb',
                datastore: new Datastore(),
                bucket: {
                    isShutdown: false
                }
            };
            mrstore.resources = {
                type: 'nedb',
                datastore: new Datastore(),
                bucket: {
                    isShutdown: false
                }
            };
            break;
        case "couchbase":
            mrstore.users = {
                type: 'couchbase',
                bucket: new couchbase.Connection(config.mrstore.users)
            };
            mrstore.resources = {
                type: 'couchbase',
                bucket: new couchbase.Connection(config.mrstore.resources)
            };
            break;
        default:
            throw new Error("Invalid connection type: " + connection.type);
    }

    return mrstore;
};

var elasticsearchConnect = function(config) {
    if (config.elasticsearch.mock === true) {
        return function() {
            return {
                cluster: {
                    health: function(options, callback) {
                        callback(null, { status: "test" });
                    }
                }
            };
        };
    } else {
        return new elasticsearch.Client(config.elasticsearch);
    }
};

var db = {};

db.setup = function(config) {
    db.mrstore = mrstoreConnect(config);
    db.elasticsearch = elasticsearchConnect(config);
};

module.exports = db;