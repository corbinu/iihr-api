var Hapi = require('hapi');

var server = {};

server.controllers = require('./controllers');
server.auth = require('./auth');

server.setup = function(config, store) {
	server.auth.setup(config, store);
	server.controllers.setup(config, store);

	server.instance = new Hapi.Server(config.hostname, config.port, {
		cors: true
	});

	server.instance.pack.register([
			{
				plugin: require('good'),
				options: config.good
			},
			{
				plugin: require('lout'),
				options: config.lout
			}
		],
		function (){}
	);

	server.instance.auth.scheme('iihr', server.auth.scheme);

	server.instance.auth.strategy('default', 'iihr');

	server.instance.route([
        {
            method: 'GET',
            path: '/',
            config: {
                handler: function(request, reply) {
                    reply.redirect(config.base + '/');
                }
            }
        },
		{
			method: 'GET',
			path: config.base + '/',
			config: server.controllers.base.config
		},
		{
			method: [ 'GET', 'POST' ],
			path: config.base + '/resources',
			config: server.controllers.resources.config
		},
		{
			method: [ 'GET', 'PUT', 'DELETE' ],
			path: config.base + '/resources/{id}',
			config: server.controllers.resource.config
		},
		{
			method: [ 'GET', 'POST' ],
			path: config.base + '/users',
			config: server.controllers.users.config
		},
		{
			method: [ 'GET', 'PUT', 'DELETE' ],
			path: config.base + '/users/{id}',
			config: server.controllers.user.config
		},
		{
			method: 'POST',
			path: config.base + '/files',
			config: server.controllers.files.config
		},
		{
			method: 'GET',
			path: config.base + '/files/{filename}',
			config: server.controllers.file.config
		},
		{
			method: 'GET',
			path: config.base + '/export.csv',
			config: server.controllers.csv.config
		},
		{
			method: 'GET',
			path: config.base + '/export.json',
			config: server.controllers.json.config
		},
		{
			method: 'POST',
			path: config.base + '/login',
			config: server.controllers.login.config
		},
		{
			method: 'GET',
			path: config.base + '/status',
			config: server.controllers.status.config
		}
	]);
};

server.start = function(callback) {

    server.instance.start(function () {
        callback(server.instance.info);
    });

};

module.exports = server;