var Hapi = require('hapi');

var auth = {};

auth.setup = function(config, store) {
	auth.scheme = function() {

        var scheme = {};

        scheme.authenticate = function(request, reply) {
            if (request.headers.authorization) {
                var auth = request.headers.authorization.split(',');
                var userId = auth[0].replace("User:", "");
                var key = auth[1].replace("Key:", "");

                store.models.user.get(userId, function(err, user) {
                    if (err) return reply(Hapi.error.internal('Auth user not found', err));

                    if (user.key === key) {
                        delete user.password;
                        return reply(null, { credentials: user });
                    }

                    return reply(Hapi.error.unauthorized('Invalid key or user', 'Key'));

                });
                
            } else {
                return reply(Hapi.error.unauthorized('No authorization header', 'Key'));
            }
        };

        return scheme;
    };
};

module.exports = auth;