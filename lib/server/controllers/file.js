var Hapi = require('hapi');
var Imagemin = require('imagemin');
var fs = require('fs');

var image_exts = [ "gif", "jpg", "jpeg", "png", "svg" ];

var fileController = {
	config: {
		auth: false
	}
};

fileController.setup = function(config, store) {

	fileController.config.handler = function (request, reply) {
		var filename = request.params.filename;
		var name_array = request.params.filename.split('.');
		var ext = name_array.pop().toLowerCase();

        if (image_exts.indexOf(ext) !== -1) {
            var base_filename = filename;
            name_array.push('min', ext);
            filename = name_array.join('.');

            if ( ! fs.existsSync(config.uploads + "/" + filename)) {
                var imagemin = new Imagemin().src(base_filename).dest(config.uploads + "/" + filename);

                switch (ext) {
                    case "gif":
                        imagemin.use(Imagemin.gifsicle({ progressive: true }));
                        break;
                    case "jpg":
                        imagemin.use(Imagemin.jpegtran({ progressive: true }));
                        break;
                    case "jpeg":
                        imagemin.use(Imagemin.jpegtran({ progressive: true }));
                        break;
                    case "png":
                        imagemin.use(Imagemin.optipng({ optimizationLevel: 3 }));
                        break;
                    case "svg":
                        imagemin.use(Imagemin.svgo());
                        break;
                }

                imagemin.optimize(function (err, file) {
                    if (err) return reply(Hapi.error.internal('Image compression error', err));

                    fs.writeFile(config.uploads + "/" + filename, file, {}, function(err) {
                        if (err) return reply(Hapi.error.internal('Write compressed image error', err));

                        return reply.file(config.uploads + "/" + filename);
                    });
                });

            } else {
                return reply.file(config.uploads + "/" + filename);
            }
        } else {
            return reply.file(config.uploads + "/" + filename);
        }

	};
};

module.exports = fileController;