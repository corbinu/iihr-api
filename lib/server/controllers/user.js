var Hapi = require('hapi');

var userController = {
	config: {
		auth: 'default'
	}
};

userController.setup = function(config, store) {

	userController.config.handler = function (request, reply) {
		var id = request.params.id;

		if (request.method === 'get') {

			store.models.user.get(id, function(err, user) {
				if (err) return reply(Hapi.error.internal('DB read error', err));
                if ( ! user) return reply(Hapi.error.notFound('No user found with the id: ' + id));

				if ( ! store.models.user.checkRole([ "admin" ], [ user.id ], request.auth.credentials)) {
					return reply(Hapi.error.unauthorized('Insufficient privileges', 'Key'));
				}

                user = user.serialize();
                delete user.password;
                delete user.key;

                return reply({ user: user });
			});

		} else if (request.method === 'delete') {

			store.models.user.get(id, function(err, user) {
				if (err) return reply(Hapi.error.internal('DB check credentials error', err));

				if ( ! store.models.user.checkRole([ "admin" ], [ user.id ], request.auth.credentials)) {
					return reply(Hapi.error.unauthorized('Insufficient privileges', 'Key'));
				}

				store.models.user.remove(id, function(err) {
					if (err) return reply(Hapi.error.internal('DB remove error', err));

					return reply(true);
				});
			});

		} else if (request.method === 'put') {
			var doc = request.payload.user;
            doc.id = id;

			store.models.user.get(id, function(err, user) {
				if (err) return reply(Hapi.error.internal('DB check credentials error', err));

				if ( ! store.models.user.checkRole([ "admin" ], [ user.id ], request.auth.credentials)) {
					return reply(Hapi.error.unauthorized('Insufficient privileges', 'Key'));
				}

				store.models.user.update(doc, function(err, user) {
                    if (err) return reply(Hapi.error.internal('DB update error', err));

                    user = user.serialize();
                    delete user.password;
                    delete user.key;
                    return reply({ user: user });
                });
			});
		}
	};
};

module.exports = userController;