var Hapi = require('hapi');

var fs = require('fs');

var filesController = {
	config: {
        auth: 'default',
		payload: {
			maxBytes: 209715200,
			output:'file',
			parse: false
		}
	}
};

filesController.setup = function(config, store) {

	filesController.config.handler = function (request, reply) {

		fs.readFile(request.payload.path, function(err, data) {
            if (err) return reply(Hapi.error.internal('File read error', err));

			var filePath = config.uploads + "/" + request.query.filename;
			fs.writeFile(filePath, data, function(err) {
				if (err) return reply(Hapi.error.internal('File save error', err));

				reply({ "filename": request.query.filename });
			});
		});

	};
};

module.exports = filesController;