var Hapi = require('hapi');
var utils = require('../../utils');

var json2csv = require('json-csv');
var fs = require('fs');

var csvController = {
	config: {
		auth: false
	}
};

csvController.setup = function(config, store) {

	csvController.config.handler = function (request, reply) {
		if (request.method === 'get') {

			store.models.resource.all(function(err, docs) {
				if (err) return reply(Hapi.error.internal('DB remove error', err));

				var fields = [
					{
						name: 'ref',
						label: 'Ref'
					},
					{
						name: 'name',
						label: 'Name'
					},
					{
						name: 'type',
						label: 'Type'
					},
					{
						name: 'other_names',
						label: 'Other_Names',
						filter: function(value) {
							return value.join('\\n');
						}
					},
					{
						name: 'location.address',
						label: 'Location_Address',
						filter: function(value) {
							return utils.stringEncode(value);
						}
					},
					{
						name: 'location.description',
						label: 'Location_Description',
						filter: function(value) {
							return utils.stringEncode(value);
						}
					},
					{
						name: 'location.geojson',
						label: 'Location_Geojson',
						filter: function(value) {
							return utils.stringEncode(JSON.stringify(value, null, '\t'));
						}
					},
					{
						name: 'description',
						label: 'Description',
						filter: function(value) {
							return utils.stringEncode(value);
						}
					},
					{
						name: 'classification',
						label: 'Classification'
					},
					{
						name: 'dating',
						label: 'Dating'
					},
					{
						name: 'materials',
						label: 'Materials'
					},
					{
						name: 'condition',
						label: 'Condition'
					},
					{
						name: 'designation',
						label: 'Designation'
					},
					{
						name: 'threats',
						label: 'Threats',
						filter: function(value) {
                            if (Object.prototype.toString.call(value) === '[object Array]') {
                                var threats = "";
                                value.forEach(function(threat) {
                                    threats += "Name: " + threat.name + '\\n';
                                    threats += "Date: " + threat.date + '\\n';
                                    threats += "Description: " + threat.description + '\\n\\n';
                                });
                                return threats;
                            } else {
                                return value;
                            }

						}
					},
					{
						name: 'measurements',
						label: 'Measurements'
					},
					{
						name: 'references',
						label: 'References',
						filter: function(value) {
                            if (Object.prototype.toString.call(value) === '[object Array]') {
                                var references = "";
                                value.forEach(function(reference) {
                                    references +=  reference.name + ": " + reference.href + '\\n';
                                });
                                return references;
                            } else {
                                return value;
                            }
						}
					},
					{
						name: 'author',
						label: 'Author'
					},
					{
						name: 'created',
						label: 'Created'
					},
					{
						name: 'updates',
						label: 'Updates',
						filter: function(value) {
                            if (Object.prototype.toString.call(value) === '[object Array]') {
                                var updates = "";
                                value.forEach(function(update) {
                                    updates +=  update.author + ": " + update.time + '\\n';
                                });
                                return updates;
                            } else {
                                return value;
                            }
						}
					}
				];

				json2csv.toCSV({ data: docs, fields: fields }, function(err, csv) {
                    if (err) return reply(Hapi.error.internal('JSON to CSV error', err));

					fs.writeFile("/tmp/csv_export", csv, function(err) {
						if (err) return reply(Hapi.error.internal('Save export error', err));

						return reply.file("/tmp/csv_export", {
							filename: "export.csv",
							mode: 'attachment'
						});
					});
				});

			});

		}
	};

};

module.exports = csvController;