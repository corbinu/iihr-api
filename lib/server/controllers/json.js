var Hapi = require('hapi');
var fs = require('fs');

var jsonController = {
	config: {
		auth: false
	}
};

jsonController.setup = function(config, store) {

	jsonController.config.handler = function (request, reply) {
		if (request.method === 'get') {

			store.models.resource.all(function(err, docs) {
				if (err) return reply(Hapi.error.internal('DB remove error', err));

				fs.writeFile("/tmp/docs_export", docs, function(err) {
					if (err) return reply(Hapi.error.internal('Save export error', err));

					return reply.file("/tmp/docs_export", {
						filename: "export.json",
						mode: 'attachment'
					});
				});

			});

		}
	};

};

module.exports = jsonController;