var Hapi = require('hapi');

var usersController = {
	config: {
		auth: {
			strategy: 'default',
			mode: 'try'
		}
	}
};

usersController.setup = function(config, store) {

	usersController.config.handler = function (request, reply) {
		if (request.method === 'post') {
			var doc = request.payload.user;

            if (doc) {
                store.models.user.add(doc, function (err, user) {
                    if (err) {
                        if (err.message.indexOf("already exists") > 0) {
                            var error = Hapi.error.badRequest(err);
                            error.output.statusCode = 409;
                            error.reformat();

                            return reply(error);
                        }
                        return reply(Hapi.error.internal('DB insert error', err));
                    }

                    user = user.serialize();
                    delete user.key;
                    delete user.password;

                    return reply({ user: user });
                });
            } else {
                var docs = request.payload.users;

                store.models.user.insert(docs, function(err, users) {
                    if (err) {
                        if (err.message.indexOf("already exists") > 0) {
                            var error = Hapi.error.badRequest(err);
                            error.output.statusCode = 409;
                            error.reformat();

                            return reply(error);
                        }
                        return reply(Hapi.error.internal('DB insert error', err));
                    }

                    users = users.map(function (user) {
                        user.serialize();
                        delete user.key;
                        delete user.password;
                        return user;
                    });

                    return reply({ users: users });
                });
            }

		} else if (request.method === 'get') {

			if ( ! request.auth.isAuthenticated) {
				return reply(Hapi.error.unauthorized('No authorization header', 'Key'));
			}

			if ( ! store.models.user.checkRole([ "admin" ], [], request.auth.credentials)) {
				return reply(Hapi.error.unauthorized('Insufficient privileges', 'Key'));
			}

            store.models.user.page(request.query.limit, request.query.page, function(err, result) {
                if (err) return reply(Hapi.error.internal('DB get error', err));

                var users = result.docs.map(function (user) {
                    user.serialize();
                    delete user.password;
                    delete user.key;
                    return user;
                });

                return reply({
                    users: users,
                    meta: {
                        total: result.total
                    }
                });
            });
		}
	};

};

module.exports = usersController;