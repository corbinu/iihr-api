var Hapi = require('hapi');

var resourceController = {
	config: {
		auth: {
			strategy: 'default',
			mode: 'try'
		}
	}
};

resourceController.setup = function(config, store) {

	resourceController.config.handler = function (request, reply) {
		var id = request.params.id;

		if (request.method === 'get') {

            var replyResource = function(err, resource) {
                if (err) return reply(Hapi.error.internal('DB read error', err));
                if ( ! resource) return reply(Hapi.error.notFound('No resource found with the id: ' + id));

                return reply({ resource: resource.serialize() });
            };

            if (isNaN(Number(id))) {
                store.models.resource.get(id, replyResource);
            } else {
                store.models.resource.byRef(Number(id), replyResource);
            }

        } else if (request.method === 'delete') {

			if ( ! request.auth.isAuthenticated) {
				return reply(Hapi.error.unauthorized('No authorization header', 'Key'));
			}

            if ( ! store.models.user.checkRole([ "manager" ], [], request.auth.credentials)) {
                return reply(Hapi.error.unauthorized('Insufficient privileges', 'Key'));
            }

            store.models.resource.remove(id, function(err) {
                if (err) return reply(Hapi.error.internal('DB remove error', err));

                return reply(true);
            });

		} else if (request.method === 'put') {
			var doc = request.payload.resource;
            doc.id = id;
            doc.created = new Date(doc.created);

			if ( ! request.auth.isAuthenticated) {
				return reply(Hapi.error.unauthorized('No authorization header', 'Key'));
			}

			store.models.resource.get(id, function(err, resource) {
				if (err) return reply(Hapi.error.internal('DB check credentials error', err));

				if ( ! store.models.user.checkRole([ "manager" ], [ resource.author ], request.auth.credentials)) {
					return reply(Hapi.error.unauthorized('Insufficient privileges', 'Key'));
				}

				store.models.resource.upsert(doc, function(err, resource) {
					if (err) return reply(Hapi.error.internal('DB update error', err));

					return reply({ resource: resource.serialize() });
				});
			});
		}
	};

};

module.exports = resourceController;