var Hapi = require('hapi');

var loginController = {
    config: {
        auth: false
    }
};

loginController.setup = function(config, store) {

	loginController.config.handler = function (request, reply) {

        if ( ! request.payload.username || ! request.payload.password) {
            return reply(Hapi.error.badRequest('Missing username or password'));
        } else {
            store.models.user.byUsername(request.payload.username, function(err, user) {
                if (err) return reply(Hapi.error.internal('DB login error', err));

                var password = "";
                if (user.password) {
                    password = user.password;
                }

                store.models.user.validate(password, request.payload.password, function(err, isValid) {
                    if (err) return reply(Hapi.error.internal('User validation error', err));

                    if ( ! isValid) {
                        return reply(Hapi.error.badRequest('Invalid username or password'));
                    }

                    delete user.password;
                    reply(user);
                });
            });
        }
        
	};
};

module.exports = loginController;