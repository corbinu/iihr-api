var controllers = {};

controllers.base = require('./base');
controllers.csv = require('./csv');
controllers.file = require('./file');
controllers.files = require('./files');
controllers.json = require('./json');
controllers.login = require('./login');
controllers.resource = require('./resource');
controllers.resources = require('./resources');
controllers.status = require('./status');
controllers.user = require('./user');
controllers.users = require('./users');

controllers.setup = function(config, store) {
	controllers.base.setup(config, store);
    controllers.csv.setup(config, store);
    controllers.file.setup(config, store);
    controllers.files.setup(config, store);
    controllers.json.setup(config, store);
	controllers.login.setup(config, store);
    controllers.resource.setup(config, store);
    controllers.resources.setup(config, store);
	controllers.status.setup(config, store);
	controllers.user.setup(config, store);
    controllers.users.setup(config, store);
};

module.exports = controllers;