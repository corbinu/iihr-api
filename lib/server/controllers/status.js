var Hapi = require('hapi');

var statusController  = {
	config: {
		auth: 'default'
	}
};

statusController.setup = function(config, store) {

	statusController.config.handler = function (request, reply) {

		if ( ! store.models.user.checkRole([ "admin" ], [], request.auth.credentials)) {
			return reply(Hapi.error.unauthorized('Insufficient privileges', 'Key'));
		}

		var serverStatus = {};

		var client = store.db.elasticsearch();

		client.cluster.health({}, function (err, response) {
			if (err) {
				serverStatus.elasticsearch = 'error';
			} else {
				serverStatus.elasticsearch = response.status;
			}

            serverStatus.couchbase = {
                users: store.db.mrstore.users.bucket.isShutdown,
                data: store.db.mrstore.users.bucket.isShutdown
            };

            reply(serverStatus);
		});
	};
};

module.exports = statusController;