var Hapi = require('hapi');

var resourcesController = {
    config: {
        auth: {
            strategy: 'default',
            mode: 'try'
        }
    }
};

resourcesController.setup = function(config, store) {

	resourcesController.config.handler = function (request, reply) {
		if (request.method === 'post') {
			var doc = request.payload.resource;

			if ( ! request.auth.isAuthenticated) {
				return reply(Hapi.error.unauthorized('No authorization header', 'Key'));
			}

            if (doc) {
                doc.author = request.auth.credentials.id;

                if (doc.name === undefined || doc.name === "") {
                    return reply(Hapi.error.badRequest('No name'));
                }

                if (! isNaN(Number(doc.name))) {
                    return reply(Hapi.error.badRequest('Invalid name'));
                }

                store.models.resource.insert(doc, function(err, resource) {
                    if (err) {
                        if (err.message.indexOf("already exists") > 0) {
                            var error = Hapi.error.badRequest(err);
                            error.output.statusCode = 409;
                            error.reformat();

                            return reply(error);
                        }
                        return reply(Hapi.error.internal('DB insert error', err));
                    }

                    return reply({ resource: resource.serialize() });
                });
            } else {
                var docs = request.payload.resources;

                if ( ! store.models.user.checkRole([ "manager" ], [], request.auth.credentials)) {
                    return reply(Hapi.error.unauthorized('Insufficient privileges', 'Key'));
                }

                var errs = [];

                docs.forEach(function(doc) {
                    doc.author = request.auth.credentials.id;

                    if (doc.name === undefined || doc.name === "") {
                        errs.push({
                            err: "No name",
                            doc: doc
                        });
                    }

                    if (! isNaN(Number(doc.name))) {
                        errs.push({
                            err: "Invalid name",
                            doc: doc
                        });
                    }

                });

                if (errs.length > 0) {
                    return reply(Hapi.error.badRequest('Invalid items', errs));
                }

                store.models.resource.insert(docs, function(err, resources) {
                    if (err) {
                        if (err.message.indexOf("already exists") > 0) {
                            var error = Hapi.error.badRequest(err);
                            error.output.statusCode = 409;
                            error.reformat();

                            return reply(error);
                        }
                        return reply(Hapi.error.internal('DB insert error', err));
                    }

                    resources = resources.map(function (resource) {
                        return resource.serialize();
                    });

                    return reply({ resources: resources });
                });
            }

		} else if (request.method === 'get') {

			var respond = function(err, result) {
				if (err) return reply(Hapi.error.internal('DB get error', err));

                var resources = result.docs.map(function (resource) {
                    return resource.serialize();
                });

				return reply({
                    resources: resources,
                    meta: {
                        total: result.total
                    }
                });
			};

			if (request.query.term && request.query.term !== "") {
				store.models.resource.search(request.query.term, request.query.limit, request.query.page, respond);
			} else {
				store.models.resource.page(request.query.limit, request.query.page, respond);
			}

		}
	};

};

module.exports = resourcesController;