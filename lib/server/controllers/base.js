var baseController = {
	config: {
		auth: {
			strategy: 'default',
			mode: 'try'
		}
	}
};

baseController.setup = function(config, store) {

	baseController.config.handler = function (request, reply) {
		if (request.auth.credentials) {
			reply({
                'hello': 'Welcome ' + request.auth.credentials.username + ' to the ' + config.name +  ' API',
				'version': 'v0'
			});
		} else {
			reply({
				'hello': 'Welcome to the ' + config.name +  ' API',
				'version': 'v0'
			});
		}
	};

};

module.exports = baseController;