var iihr = {};

iihr.config = require('./config');
iihr.store = require('./store');
iihr.server = require('./server');

iihr.setup = function() {
	iihr.store.setup(iihr.config);
	iihr.server.setup(iihr.config, iihr.store);
};

iihr.start = function(callback) {
    iihr.store.start(function(err) {
        if (err) throw err;

        iihr.server.start(callback);
    });
};

module.exports = iihr;