var Lab = require('lab');

var utils = require('../lib/utils');

var lab = exports.lab = Lab.script();

lab.experiment('Utils', function () {

    lab.test('convert string to slug', function (done) {

        Lab.expect(utils.slug("hello@world.test_hi")).to.equal("hello-world-test-hi");

        done();
    });

	lab.test('convert JS Object to pretty string', function (done) {

		Lab.expect(utils.objPrettyString({
			test: "test",
			subtest: {
				subprop: "test prop"
			},
			arraytest: ["one", "two", "three"]
		})).to.equal('{\n\t"test": "test",\n\t"subtest": {\n\t\t"subprop": "test prop"\n\t},\n\t"arraytest": [\n\t\t"one",\n\t\t"two",\n\t\t"three"\n\t]\n}');

		done();
	});

	lab.test('escape a string', function (done) {

		Lab.expect(utils.stringEncode('\n\r\'"\\\t\b\f/<>')).to.equal("\\n\\r\\'\\\"\\\\\\t\\b\\f\\x2F\\x3C\\x3E");

		done();
	});

});
