var Lab = require('lab');

var config = require('../lib/config');

var lab = exports.lab = Lab.script();

lab.experiment('Config', function () {

    lab.test('test config', function (done) {

        Lab.expect(config).to.have.property('name', 'IIHR');
        Lab.expect(config).to.have.property('base', '/api/v0');
        Lab.expect(config).to.have.property('port', 4000);
        Lab.expect(config).to.have.deep.property('good.subscribers.console').that.is.an('array').length(0);
        Lab.expect(config).to.have.deep.property('lout.endpoint', '/api/docs');
        Lab.expect(config).to.have.property('env', 'test');

        done();
    });

});