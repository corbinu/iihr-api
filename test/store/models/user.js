var Lab = require('lab');

var bcrypt = require('bcrypt');

var API = require('../../../lib');
var User = require('../../../lib/store/models/user');

var lab = exports.lab = Lab.script();

lab.experiment('User model', function () {

    lab.before(function (done) {

        API.setup();
        API.store.start(done);
    });

    lab.test('setup', function (done) {

        User.setup(API.config, API.store.db);

        done();
    });

    lab.experiment('internal', function () {
        lab.test('hash password', function (done) {

            API.store.models.user.internal.hash_pass("password", API.config.password_salt, function (err, hash) {
                if (err) return done(err);

                bcrypt.compare(API.config.password_salt + "password", hash, function (err, isValid) {
                    if (err) return done(err);

                    Lab.expect(isValid).to.equal(true);

                    done();
                });
            });
        });

        lab.test('generate Key', function (done) {

            var key = API.store.models.user.internal.gen_key("password", API.config.key_salt);
            Lab.expect(key).to.be.a('string');

            done();
        });

    });

    lab.test('basic', function (done) {

        Lab.expect(User).to.have.property('setup').that.is.an('function');
        Lab.expect(User).to.have.property('start').that.is.an('function');

        done();
    });

});
