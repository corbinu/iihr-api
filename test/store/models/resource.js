var Lab = require('lab');

var API = require('../../../lib');
var Resource = require('../../../lib/store/models/resource');

var lab = exports.lab = Lab.script();

lab.experiment('Resource model', function () {

    lab.test('basic', function (done) {

        Lab.expect(Resource).to.have.property('setup').that.is.an('function');
        Lab.expect(Resource).to.have.property('start').that.is.an('function');

        done();
    });

    lab.test('setup', function (done) {

        Resource.setup(API.config, API.store.db);

        done();
    });

    lab.test('search "quincy" page 0', { skip: true }, function (done) {
        // TODO once this works add to /resources tests

        API.store.models.resource.search("quincy", 2, 0, function(err, resources, total) {
            if (err) return done(err);

            Lab.expect(resources).to.be.an('array');
            Lab.expect(resources).to.have.length(2);
            Lab.expect(total).to.equal(3);

            done();
        });
    });

    lab.test('search "quincy" page 1', { skip: true }, function (done) {
        // TODO once this works add to /resources tests

        API.store.models.resource.search("quincy", 2, 1, function(err, resources, total) {
            if (err) return done(err);

            Lab.expect(resources).to.be.an('array');
            Lab.expect(resources).to.have.length(1);
            Lab.expect(total).to.equal(3);

            done();
        });
    });

    lab.test('search "apple" page 0', { skip: true }, function (done) {
        // TODO once this works add to /resources tests
        API.store.models.resource.search("apple", 2, 0, function(err, resources, total) {
            if (err) return done(err);

            Lab.expect(resources).to.be.an('array');
            Lab.expect(resources).to.have.length(0);
            Lab.expect(total).to.equal(0);

            done();
        });
    });

});
