var Lab = require('lab');

var API = require('../../../lib');
var models = require('../../../lib/store/models');

var lab = exports.lab = Lab.script();

lab.experiment('Models', function () {

    lab.test('basic', function (done) {

        Lab.expect(models).to.have.property('resource');
        Lab.expect(models).to.have.property('user');
        Lab.expect(models).to.have.property('setup').that.is.an('function');
        Lab.expect(models).to.have.property('start').that.is.an('function');

        done();
    });

    lab.test('setup', function (done) {

        models.setup(API.config, API.store.db);

        done();
    });

    lab.test('start', function (done) {

        models.start(done);
    });

});