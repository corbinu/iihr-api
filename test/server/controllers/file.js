var Lab = require('lab');
var fs = require('fs');

var API = require('../../../lib');
var fileController = require('../../../lib/server/controllers/file');

var lab = exports.lab = Lab.script();

lab.experiment('Files', function () {

    lab.before(function(done) {
        fs.readdir('./test/upload', function (err, files) {
            if (err) throw err;

            files.forEach(function(file) {
                fs.createReadStream('./test/upload/' + file).pipe(fs.createWriteStream('./test/content/' + file));
            });
            done();
        });
    });

    lab.test('basic', function (done) {

        Lab.expect(fileController).to.have.property('setup').that.is.an('function');

        done();
    });

    lab.test('setup', function (done) {

        fileController.setup(API.config, API.store);

        done();
    });

    lab.test('get pdf', function (done) {
        var filename = "89001095.pdf";

        var options = {
            method: "GET",
            url: API.config.base + '/files/' + filename
        };

        API.server.instance.inject(options, function(response) {
            Lab.expect(response.statusCode).to.equal(200);

            done();
        });
    });

    lab.test('get jpeg', function (done) {
        var filename = "quincy-mine-no-2-shaft-hoist-house-Shaft_No_2.jpg";

        var options = {
            method: "GET",
            url: API.config.base + '/files/' + filename
        };

        API.server.instance.inject(options, function(response) {
            Lab.expect(response.statusCode).to.equal(200);

            done();
        });
    });

    lab.test('get gif', function (done) {
        var filename = "dredge3.gif";

        var options = {
            method: "GET",
            url: API.config.base + '/files/' + filename
        };

        API.server.instance.inject(options, function(response) {
            Lab.expect(response.statusCode).to.equal(200);

            done();
        });
    });

    lab.after(function(done) {
        fs.readdir('./test/content', function (err, files) {
            if (err) throw err;

            files.forEach(function(file) {
                fs.unlinkSync("./test/content/" + file);
            });
            done();
        });
    });

});