var Lab = require('lab');

var API = require('../../../lib');
var helpers  = require('../../helpers');
var loginController = require('../../../lib/server/controllers/login');

var lab = exports.lab = Lab.script();

lab.experiment('Login', function () {

    lab.before(function (done) {

        API.setup();
        API.store.start(function (err) {
            if (err) return done(err);

            API.store.models.user.reset(function(err) {
                if (err) return done(err);

                API.store.models.user.all(done);

            });
        });
    });

    lab.test('basic', function (done) {

        Lab.expect(loginController).to.have.property('setup').that.is.an('function');

        done();
    });

    lab.test('setup', function (done) {

        loginController.setup(API.config, API.store);

        done();
    });

    lab.test('no password', function (done) {

        var options = {
            method: "POST",
            url: API.config.base + '/login',
            payload: { username: "demo" }
        };

        API.server.instance.inject(options, function(response) {
            var result = response.result;

            Lab.expect(response.statusCode).to.equal(400);
            Lab.expect(result).to.be.a('object');
            Lab.expect(result).to.have.property('statusCode', 400);
            Lab.expect(result).to.have.property('error', 'Bad Request');
            Lab.expect(result).to.have.property('message', 'Missing username or password');

            done();
        });
    });

    lab.test('no username', function (done) {

        var options = {
            method: "POST",
            url: API.config.base + '/login',
            payload: { password: "password" }
        };

        API.server.instance.inject(options, function(response) {
            var result = response.result;

            Lab.expect(response.statusCode).to.equal(400);
            Lab.expect(result).to.be.a('object');
            Lab.expect(result).to.have.property('statusCode', 400);
            Lab.expect(result).to.have.property('error', 'Bad Request');
            Lab.expect(result).to.have.property('message', 'Missing username or password');

            done();
        });
    });

    lab.test('invalid password', function (done) {

        var options = {
            method: "POST",
            url: API.config.base + '/login',
            payload: { username: "demo", password: "bs" }
        };

        API.server.instance.inject(options, function(response) {
            var result = response.result;

            Lab.expect(response.statusCode).to.equal(400);
            Lab.expect(result).to.be.a('object');
            Lab.expect(result).to.have.property('statusCode', 400);
            Lab.expect(result).to.have.property('error', 'Bad Request');
            Lab.expect(result).to.have.property('message', 'Invalid username or password');

            done();
        });
    });

    lab.test('valid', function (done) {

        var options = {
            method: "POST",
            url: API.config.base + '/login',
            payload: { username: "demo", password: "password" }
        };

        API.server.instance.inject(options, function(response) {
            var result = response.result;

            Lab.expect(response.statusCode).to.equal(200);
            Lab.expect(result).to.be.a('object');

            helpers.validators.users.demoWithKey(result);

            done();
        });
    });

});
