var Lab = require('lab');

var API = require('../../../lib');
var helpers  = require('../../helpers');
var resourcesController = require('../../../lib/server/controllers/resources');

var lab = exports.lab = Lab.script();

lab.experiment('Resources', function () {

    lab.test('basic', function (done) {

        Lab.expect(resourcesController).to.have.property('setup').that.is.an('function');

        done();
    });

    lab.test('setup', function (done) {

        resourcesController.setup(API.config, API.store);

        done();
    });

    lab.experiment('POST', function() {

        lab.before(function (done) {

            API.setup();
            API.store.start(function (err) {
                if (err) return done(err);

                API.store.models.resource.removeAll(done);
            });
        });

        lab.test('no login add', function (done) {

            var options = {
                method: "POST",
                url: API.config.base + '/resources',
                payload: { resource: helpers.data.resources[0] }
            };

            API.server.instance.inject(options, function (response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(401);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('statusCode', 401);
                Lab.expect(result).to.have.property('error', 'Unauthorized');
                Lab.expect(result).to.have.property('message', 'No authorization header');

                done();
            });
        });

        lab.test('logged in add', function (done) {

            var options = {
                method: "POST",
                url: API.config.base + '/resources',
                payload: { resource: helpers.data.resources[0] },
                credentials: {
                    "id": "demo2-example-com",
                    "username": "demo2",
                    roles: [ ]
                }
            };

            API.server.instance.inject(options, function (response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(200);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('resource');
                helpers.validators.resources.quincyMine(result.resource);

                done();
            });
        });

        lab.test('logged in authorized add no name', function (done) {

            var options = {
                method: "POST",
                url: API.config.base + '/resources',
                payload: {
                    resource: {
                        type: "Bad"
                    }
                },
                credentials: {
                    "id": "demo2-example-com",
                    "username": "demo2",
                    roles: [ ]
                }
            };

            API.server.instance.inject(options, function (response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(400);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('statusCode', 400);
                Lab.expect(result).to.have.property('error', 'Bad Request');
                Lab.expect(result).to.have.property('message', 'No name');

                done();
            });
        });

        lab.test('logged in authorized add invalid name', function (done) {

            var options = {
                method: "POST",
                url: API.config.base + '/resources',
                payload: {
                    resource: {
                        name: "12345"
                    }
                },
                credentials: {
                    id: "demo2-example-com",
                    username: "demo2",
                    roles: [ ]
                }
            };

            API.server.instance.inject(options, function (response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(400);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('statusCode', 400);
                Lab.expect(result).to.have.property('error', 'Bad Request');
                Lab.expect(result).to.have.property('message', 'Invalid name');

                done();
            });
        });

        lab.test('logged in unauthorized add multiple', function (done) {
            var resources = helpers.data.resources.slice(1);

            var options = {
                method: "POST",
                url: API.config.base + '/resources',
                payload: { resources: resources },
                credentials: {
                    id: "demo2-example-com",
                    username: "demo2",
                    roles: []
                }
            };

            API.server.instance.inject(options, function (response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(401);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('statusCode', 401);
                Lab.expect(result).to.have.property('error', 'Unauthorized');
                Lab.expect(result).to.have.property('message', 'Insufficient privileges');

                done();
            });
        });

        lab.test('logged in authorized add multiple', function (done) {
            var resources = helpers.data.resources.slice(1);

            var options = {
                method: "POST",
                url: API.config.base + '/resources',
                payload: { resources: resources },
                credentials: {
                    id: "demo-example-com",
                    username: "demo",
                    roles: [ "manager" ]
                }
            };

            API.server.instance.inject(options, function (response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(200);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('resources');

                helpers.verifyArray(result.resources, 'id', {
                    'cliff-mine-archaeological-site': helpers.validators.resources.cliffMine,
                    'quincy-mine-no-2-shaft-hoist-house': helpers.validators.resources.quincyMineHoist,
                    'quincy-dredge-2': helpers.validators.resources.quincyDredge
                });

                done();
            });
        });

        lab.test('logged in authorized add again', function (done) {

            var options = {
                method: "POST",
                url: API.config.base + '/resources',
                payload: { resource: helpers.data.resources[0] },
                credentials: {
                    id: "demo2-example-com",
                    username: "demo2",
                    roles: [ "manager" ]
                }
            };

            API.server.instance.inject(options, function (response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(409);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('statusCode', 409);
                Lab.expect(result).to.have.property('error', 'Conflict');
                Lab.expect(result).to.have.property('message', 'Error: Resource with id quincy-mining-company-historic-district already exists');

                done();
            });
        });

        lab.test('logged in authorized add invalid name and no name', function (done) {
            var resources = JSON.parse(JSON.stringify(helpers.data.resources.slice(0)));
            resources[1].name = "1234";
            resources[2].name = "";

            var options = {
                method: "POST",
                url: API.config.base + '/resources',
                payload: { resources: resources },
                credentials: {
                    id: "demo2-example-com",
                    username: "demo2",
                    roles: [ "manager" ]
                }
            };

            API.server.instance.inject(options, function (response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(400);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('statusCode', 400);
                Lab.expect(result).to.have.property('error', 'Bad Request');
                Lab.expect(result).to.have.property('message', 'Invalid items');

                done();
            });
        });

    });

    lab.experiment('GET', function() {

        lab.test('get', function (done) {

            var options = {
                method: "GET",
                url: API.config.base + '/resources'
            };

            API.server.instance.inject(options, function(response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(200);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('resources');
                Lab.expect(result).to.have.deep.property('meta.total', 4);

                helpers.verifyArray(result.resources, 'id', {
                    'quincy-mining-company-historic-district': helpers.validators.resources.quincyMine,
                    'cliff-mine-archaeological-site': helpers.validators.resources.cliffMine,
                    'quincy-mine-no-2-shaft-hoist-house': helpers.validators.resources.quincyMineHoist,
                    'quincy-dredge-2': helpers.validators.resources.quincyDredge
                });

                done();
            });
        });

        lab.test('get pages', function (done) {

            var all = [];

            var options = {
                method: "GET",
                url: API.config.base + '/resources?limit=3&page=0'
            };

            API.server.instance.inject(options, function(response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(200);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('resources').to.be.an('array').length(3);
                Lab.expect(result).to.have.deep.property('meta.total', 4);

                all = all.concat(result.resources);

                var options = {
                    method: "GET",
                    url: API.config.base + '/resources?limit=3&page=1'
                };

                API.server.instance.inject(options, function(response) {
                    var result = response.result;

                    Lab.expect(response.statusCode).to.equal(200);
                    Lab.expect(result).to.be.a('object');
                    Lab.expect(result).to.have.property('resources').to.be.an('array').length(1);
                    Lab.expect(result).to.have.deep.property('meta.total', 4);

                    all = all.concat(result.resources);

                    helpers.verifyArray(all, 'id', {
                        'quincy-mining-company-historic-district': helpers.validators.resources.quincyMine,
                        'cliff-mine-archaeological-site': helpers.validators.resources.cliffMine,
                        'quincy-mine-no-2-shaft-hoist-house': helpers.validators.resources.quincyMineHoist,
                        'quincy-dredge-2': helpers.validators.resources.quincyDredge
                    });

                    done();
                });
            });
        });

    });

});
