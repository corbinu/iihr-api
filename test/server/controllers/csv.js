var Lab = require('lab');

var API = require('../../../lib');
var helpers  = require('../../helpers');
var csvController = require('../../../lib/server/controllers/csv');

var lab = exports.lab = Lab.script();

lab.experiment('CSV', function () {

    lab.before(function (done) {

        API.setup();
        API.store.start(function (err) {
            if (err) return done(err);

            API.store.models.user.reset(function(err) {
                if (err) return done(err);

                API.store.models.user.all(done);

            });
        });
    });

    lab.test('basic', function (done) {

        Lab.expect(csvController).to.have.property('setup').that.is.an('function');

        done();
    });

    lab.test('setup', function (done) {

        csvController.setup(API.config, API.store);

        done();
    });

    lab.test('export', function (done) {
        var options = {
            method: "GET",
            url: API.config.base + '/export.csv'
        };

        API.server.instance.inject(options, function(response) {
            Lab.expect(response.statusCode).to.equal(200);

            done();
        });
    });

});