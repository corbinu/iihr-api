var Lab = require('lab');

var API = require('../../../lib');
var helpers  = require('../../helpers');
var usersController = require('../../../lib/server/controllers/users');

var lab = exports.lab = Lab.script();

lab.experiment('Users', function () {

    lab.test('basic', function (done) {

        Lab.expect(usersController).to.have.property('setup').that.is.an('function');

        done();
    });

    lab.test('setup', function (done) {

        usersController.setup(API.config, API.store);

        done();
    });

    lab.experiment('POST', function() {

        lab.before(function (done) {

            API.setup();
            API.store.start(function (err) {
                if (err) return done(err);

                API.store.models.user.removeAll(done);
            });
        });

        lab.test('add', function (done) {

            var options = {
                method: "POST",
                url: API.config.base + '/users',
                payload: { user: helpers.data.users[0] }
            };

            API.server.instance.inject(options, function (response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(200);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('user');
                helpers.validators.users.demo(result.user);

                done();
            });
        });

        lab.test('add again', function (done) {

            var options = {
                method: "POST",
                url: API.config.base + '/users',
                payload: { user: helpers.data.users[0] }
            };

            API.server.instance.inject(options, function (response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(409);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('statusCode', 409);
                Lab.expect(result).to.have.property('error', 'Conflict');
                Lab.expect(result).to.have.property('message', 'Error: User with id demo-example-com already exists');

                done();
            });
        });

        lab.test('add multiple', function (done) {
            var users = helpers.data.users.slice(1);

            var options = {
                method: "POST",
                url: API.config.base + '/users',
                payload: { users: users }
            };

            API.server.instance.inject(options, function (response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(200);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('users');

                helpers.verifyArray(result.users, 'id', {
                    'demo2-example-com': helpers.validators.users.demo2,
                    'demo3-example-com': helpers.validators.users.demo3
                });

                done();
            });
        });

    });

    lab.experiment('GET', function() {

        lab.test('no login get', function (done) {

            var options = {
                method: "GET",
                url: API.config.base + '/users?limit=2&page=0'
            };

            API.server.instance.inject(options, function (response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(401);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('statusCode', 401);
                Lab.expect(result).to.have.property('error', 'Unauthorized');
                Lab.expect(result).to.have.property('message', 'No authorization header');

                done();
            });
        });

        lab.test('logged in unauthorized get', function (done) {

            var options = {
                method: "GET",
                url: API.config.base + '/users?limit=2&page=0',
                credentials: {
                    "id": "demo2-example-com",
                    "username": "demo2",
                    roles: [ "manager" ]
                }
            };

            API.server.instance.inject(options, function (response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(401);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('statusCode', 401);
                Lab.expect(result).to.have.property('error', 'Unauthorized');
                Lab.expect(result).to.have.property('message', 'Insufficient privileges');

                done();
            });
        });

        lab.test('logged in authorized get', function (done) {

            var options = {
                method: "GET",
                url: API.config.base + '/users',
                credentials: {
                    "id": "demo2-example-com",
                    "username": "demo2",
                    roles: [ "manager", "admin" ]
                }
            };

            API.server.instance.inject(options, function(response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(200);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('users');
                Lab.expect(result).to.have.deep.property('meta.total', 3);

                helpers.verifyArray(result.users, 'id', {
                    'demo-example-com': helpers.validators.users.demo,
                    'demo2-example-com': helpers.validators.users.demo2,
                    'demo3-example-com': helpers.validators.users.demo3
                });

                done();
            });
        });

        lab.test('logged in authorized get pages', function (done) {

            var all = [];

            var options = {
                method: "GET",
                url: API.config.base + '/users?limit=2&page=0',
                credentials: {
                    "id": "demo2-example-com",
                    "username": "demo2",
                    roles: [ "manager", "admin" ]
                }
            };

            API.server.instance.inject(options, function(response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(200);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('users').to.be.an('array').length(2);
                Lab.expect(result).to.have.deep.property('meta.total', 3);

                all = all.concat(result.users);

                var options = {
                    method: "GET",
                    url: API.config.base + '/users?limit=2&page=1',
                    credentials: {
                        "id": "demo2-example-com",
                        "username": "demo2",
                        roles: [ "manager", "admin" ]
                    }
                };

                API.server.instance.inject(options, function(response) {
                    var result = response.result;

                    Lab.expect(response.statusCode).to.equal(200);
                    Lab.expect(result).to.be.a('object');
                    Lab.expect(result).to.have.property('users').to.be.an('array').length(1);
                    Lab.expect(result).to.have.deep.property('meta.total', 3);

                    all = all.concat(result.users);

                    helpers.verifyArray(all, 'id', {
                        'demo-example-com': helpers.validators.users.demo,
                        'demo2-example-com': helpers.validators.users.demo2,
                        'demo3-example-com': helpers.validators.users.demo3
                    });

                    done();
                });
            });
        });

    });

});
