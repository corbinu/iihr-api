var Lab = require('lab');

var API = require('../../../lib');
var controllers = require('../../../lib/server/controllers');

var lab = exports.lab = Lab.script();

lab.experiment('Controllers', function () {

    lab.test('basic', function (done) {

        Lab.expect(controllers).to.have.property('base');
        Lab.expect(controllers).to.have.property('login');
        Lab.expect(controllers).to.have.property('status');
        Lab.expect(controllers).to.have.property('resources');
        Lab.expect(controllers).to.have.property('resource');
        Lab.expect(controllers).to.have.property('users');
        Lab.expect(controllers).to.have.property('user');
        Lab.expect(controllers).to.have.property('files');
        Lab.expect(controllers).to.have.property('file');
        Lab.expect(controllers).to.have.property('csv');
        Lab.expect(controllers).to.have.property('json');
        Lab.expect(controllers).to.have.property('setup').that.is.an('function');

        done();
    });

    lab.test('setup', function (done) {

        controllers.setup(API.config, API.store);

        done();
    });

});