var Lab = require('lab');

var API = require('../../../lib');
var helpers  = require('../../helpers');
var resourceController = require('../../../lib/server/controllers/resource');

var lab = exports.lab = Lab.script();

lab.experiment('Resource', function () {

    var resource;
    var id;
    var created;

    var validateUpdated = function (resource) {
        Lab.expect(resource).to.have.property('id', 'quincy-mining-company-historic-district');
        Lab.expect(resource).to.have.property('ref', 1);
        Lab.expect(resource).to.have.property('type', 'Archaeological Heritage (Site)');
        Lab.expect(resource).to.have.property('name', 'Quincy Mining Company Historic District');
        Lab.expect(resource).to.have.property('other_names').to.be.an('array').length(1).to.include.members([ "Quincy Mine" ]);
        Lab.expect(resource).to.have.deep.property('location.address', 'TICCIH Address: 49750 U.S. Highway 41<br>TICCIH Postal: 49930<br>Address: Hancock<br>State: MI<br>');
        Lab.expect(resource).to.have.deep.property('location.description', 'Near Hancock, Michigan');
        Lab.expect(resource).to.have.deep.property('location.geojson.type', 'FeatureCollection');
        Lab.expect(resource).to.have.deep.property('location.geojson.features[0].type', 'Point');
        Lab.expect(resource).to.have.deep.property('location.geojson.features[0].coordinates[0]', 47.136667);
        Lab.expect(resource).to.have.deep.property('location.geojson.features[0].coordinates[1]', -88.575);
        Lab.expect(resource).to.have.property('description', "The Quincy Mine is an extensive set of copper mines located near Hancock, Michigan. The mine was owned by the Quincy Mining Company and operated between 1846 and 1945, although some activities continued through the 1970s. The Quincy Mine was known as &quot;Old Reliable,&quot; as the Quincy Mine Company paid a dividend to investors every year from 1868 through 1920. The Quincy Mining Company Historic District is a U.S. National Historic Landmark District; other Quincy Mine properties nearby, including the Quincy Mining Company Stamp Mills, the Quincy Dredge Number Two, and the Quincy Smelter are also historically significant.");
        Lab.expect(resource).to.have.property('classification', "0. EXTRACTIVE INDUSTRIES (EXTRAC)");
        Lab.expect(resource).to.have.property('dating', "1846");
        Lab.expect(resource).to.have.deep.property('threats[0].name', 'Vandalism');
        Lab.expect(resource).to.have.deep.property('threats[0].date', '04.02.2012');
        Lab.expect(resource).to.have.deep.property('threats[0].description', '4 sites in the Mining district were graffitied');
        Lab.expect(resource).to.have.property('measurements', "779 Acres");
        Lab.expect(resource).to.have.deep.property('references[0].name', 'Wiki');
        Lab.expect(resource).to.have.deep.property('references[0].href', 'http://en.wikipedia.org/wiki/Quincy_Mine');
        Lab.expect(resource).to.have.deep.property('references[1].name', 'NHL');
        Lab.expect(resource).to.have.deep.property('references[1].href', 'http://pdfhost.focus.nps.gov/docs/NHLS/Text/89001095.pdf');
        Lab.expect(resource).to.have.deep.property('references[2].name', 'TICCIH');
        Lab.expect(resource).to.have.deep.property('references[2].href', 'http://www.quincymine.com/');
        Lab.expect(resource).to.have.deep.property('references[3].name', 'HAER');
        Lab.expect(resource).to.have.deep.property('references[3].href', 'http://hdl.loc.gov/loc.pnp/hhh.mi0086');
        Lab.expect(resource).to.have.property('author', 'demo2-example-com');
        Lab.expect(resource).to.have.property('created').to.be.a('date');
        Lab.expect(resource).to.have.property('updates').to.be.an('array').length(0);
        Lab.expect(resource).to.have.property('elements').to.be.an('array').length(1).to.include.members([ 3 ]);
        Lab.expect(resource).to.have.deep.property('images[0].src', 'quincy-mining-company-historic-district-No_2_Shaft_Rock_House.jpg');
        Lab.expect(resource).to.have.deep.property('images[0].caption', 'No 2 Shaft Rock House');
        Lab.expect(resource).to.have.deep.property('images[1].src', 'quincy-mining-company-historic-district-Supply_Office_Powderhouse.jpg');
        Lab.expect(resource).to.have.deep.property('images[1].caption', 'Supply Office (background) and Powderhouse');
        Lab.expect(resource).to.have.deep.property('images[2].src', 'quincy-mining-company-historic-district-QuincyMineNo2Shafthouse.jpg');
        Lab.expect(resource).to.have.deep.property('images[2].caption', 'The #2 Shafthouse (left) and the Hoist House (right)');
        Lab.expect(resource).to.have.deep.property('documents.[0].title', 'Nomination Form');
        Lab.expect(resource).to.have.deep.property('documents.[0].src', 'quincy-mining-company-historic-district-89001095.pdf');
        Lab.expect(resource).to.have.deep.property('documents.[0].description', 'National Register of Historic Places Inventory - Nomination Form');
    };

    lab.before(function (done) {

        API.setup();
        API.store.start(function (err) {
            if (err) return done(err);

            API.store.models.resource.reset(function(err) {
                if (err) return done(err);

                API.store.models.resource.all(function(err, results) {
                    if (err) return done(err);

                    results.forEach(function(result) {
                        if (result.id === 'quincy-mining-company-historic-district') {
                            resource = result.serialize();
                            id = resource.id;
                            delete resource.id;
                        }
                    });

                    done();
                });

            });
        });
    });

    lab.test('basic', function (done) {

        Lab.expect(resourceController).to.have.property('setup').that.is.an('function');

        done();
    });

    lab.test('setup', function (done) {

        resourceController.setup(API.config, API.store);

        done();
    });

    lab.experiment('PUT', function() {

        lab.test('no login update', function (done) {

            var options = {
                method: "PUT",
                url: API.config.base + '/resources/' + id,
                payload: { resource: resource }
            };

            API.server.instance.inject(options, function(response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(401);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('statusCode', 401);
                Lab.expect(result).to.have.property('error', 'Unauthorized');
                Lab.expect(result).to.have.property('message', 'No authorization header');

                done();
            });
        });

        lab.test('logged in unauthorized update', function (done) {

            var options = {
                method: "PUT",
                url: API.config.base + '/resources/' + id,
                payload: { resource: resource },
                credentials: {
                    "id": "demo-example-com",
                    "username": "demo",
                    roles: []
                }
            };

            API.server.instance.inject(options, function(response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(401);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('statusCode', 401);
                Lab.expect(result).to.have.property('error', 'Unauthorized');
                Lab.expect(result).to.have.property('message', 'Insufficient privileges');

                done();
            });
        });

        lab.test('logged in authorized update', function (done) {
            resource.condition = "Fair";

            var options = {
                method: "PUT",
                url: API.config.base + '/resources/' + id,
                payload: { resource: resource },
                credentials: {
                    "id": "demo-example-com",
                    "username": "demo",
                    roles: [ "random", "manager", "test" ]
                }
            };

            API.server.instance.inject(options, function(response) {
                 var result = response.result;

                 Lab.expect(response.statusCode).to.equal(200);
                 Lab.expect(result).to.be.a('object');
                 Lab.expect(result).to.have.property('resource');
                 validateUpdated(result.resource);
                 Lab.expect(result.resource).to.have.property('condition', "Fair");

                 done();
            });
        });

        lab.test('logged in creator update', function (done) {
            resource.condition = "Bad";

            var options = {
                method: "PUT",
                url: API.config.base + '/resources/' + id,
                payload: { resource: resource },
                credentials: {
                    "id": "demo2-example-com",
                    "username": "demo2",
                    roles: [  ]
                }
            };

            API.server.instance.inject(options, function(response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(200);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('resource');
                validateUpdated(result.resource);
                Lab.expect(result.resource).to.have.property('condition', "Bad");

                done();
            });
        });
    });

    lab.experiment('GET', function() {

        lab.test('get by ID', function (done) {

            var options = {
                method: "GET",
                url: API.config.base + '/resources/' + id
            };

            API.server.instance.inject(options, function(response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(200);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('resource');
                validateUpdated(result.resource);
                Lab.expect(result.resource).to.have.property('condition', "Bad");

                done();
            });

        });

        lab.test('get by reference number', function (done) {

            var options = {
                method: "GET",
                url: API.config.base + '/resources/1'
            };

            API.server.instance.inject(options, function(response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(200);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('resource');
                validateUpdated(result.resource);
                Lab.expect(result.resource).to.have.property('condition', "Bad");

                done();
            });

        });

        lab.test('get nonexistant', function (done) {

            var options = {
                method: "GET",
                url: API.config.base + '/resources/doesnt-exist'
            };

            API.server.instance.inject(options, function(response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(404);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('statusCode', 404);
                Lab.expect(result).to.have.property('error', 'Not Found');
                Lab.expect(result).to.have.property('message', 'No resource found with the id: doesnt-exist');

                done();
            });

        });
    });

    lab.experiment('DELETE', function() {

        lab.test('no login remove', function (done) {

            var options = {
                method: "DELETE",
                url: API.config.base + '/resources/' + id
            };

            API.server.instance.inject(options, function(response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(401);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('statusCode', 401);
                Lab.expect(result).to.have.property('error', 'Unauthorized');
                Lab.expect(result).to.have.property('message', 'No authorization header');

                done();
            });
        });

        lab.test('logged in unauthorized remove', function (done) {

            var options = {
                method: "DELETE",
                url: API.config.base + '/resources/' + id,
                credentials: {
                    "id": "demo2-example-com",
                    "username": "demo2",
                    roles: [  ]
                }
            };

            API.server.instance.inject(options, function(response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(401);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('statusCode', 401);
                Lab.expect(result).to.have.property('error', 'Unauthorized');
                Lab.expect(result).to.have.property('message', 'Insufficient privileges');

                done();
            });
        });

        lab.test('logged in authorized remove', function (done) {
            var options = {
                method: "DELETE",
                url: API.config.base + '/resources/' + id,
                credentials: {
                    "id": "demo2-example-com",
                    "username": "demo2",
                    roles: [ "manager" ]
                }
            };

            API.server.instance.inject(options, function(response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(200);
                Lab.expect(result).to.equal(true);

                API.store.models.resource.all(function(err, resources) {
                    if (err) return done(err);

                    helpers.verifyArray(resources, 'id', {
                        'cliff-mine-archaeological-site': helpers.validators.resources.cliffMine,
                        'quincy-mine-no-2-shaft-hoist-house': helpers.validators.resources.quincyMineHoist,
                        'quincy-dredge-2': helpers.validators.resources.quincyDredge
                    });

                    done();
                });
            });

        });

    });

});
