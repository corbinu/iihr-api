var Lab = require('lab');

var API = require('../../../lib');
var statusController = require('../../../lib/server/controllers/status');

var lab = exports.lab = Lab.script();

lab.experiment('Status', function () {

    lab.before(function(done) {

        API.setup();
        API.store.start(done);
    });

    lab.test('basic', function (done) {

        Lab.expect(statusController).to.have.property('setup').that.is.an('function');

        done();
    });

    lab.test('setup', function (done) {

        statusController.setup(API.config, API.store);

        done();
    });

    lab.test('no login get', function (done) {

        var options = {
            method: "GET",
            url: API.config.base + '/status'
        };

        API.server.instance.inject(options, function(response) {
            var result = response.result;

            Lab.expect(response.statusCode).to.equal(401);
            Lab.expect(result).to.be.a('object');
            Lab.expect(result).to.have.property('statusCode', 401);
            Lab.expect(result).to.have.property('error', 'Unauthorized');
            Lab.expect(result).to.have.property('message', 'No authorization header');

            done();
        });
    });

    lab.test('logged in unauthorized get', function (done) {

        var options = {
            method: "GET",
            url: API.config.base + '/status',
            credentials: {
                "id": "demo2-example-com",
                "username": "demo2",
                roles: [ "manager" ]
            }
        };

        API.server.instance.inject(options, function(response) {
            var result = response.result;

            Lab.expect(response.statusCode).to.equal(401);
            Lab.expect(result).to.be.a('object');
            Lab.expect(result).to.have.property('statusCode', 401);
            Lab.expect(result).to.have.property('error', 'Unauthorized');
            Lab.expect(result).to.have.property('message', 'Insufficient privileges');

            done();
        });
    });

    lab.test('logged in authorized get', function (done) {

        var options = {
            method: "GET",
            url: API.config.base + '/status',
            credentials: {
                "id": "demo2-example-com",
                "username": "demo2",
                roles: [ "manager", "admin" ]
            }
        };

        API.server.instance.inject(options, function(response) {
            var result = response.result;

            Lab.expect(response.statusCode).to.equal(200);
            Lab.expect(result).to.be.a('object');
            Lab.expect(result).to.have.property('elasticsearch', 'test');
            Lab.expect(result).to.have.property('couchbase');

            done();
        });
    });

});