var Lab = require('lab');

var API = require('../../../lib');
var helpers  = require('../../helpers');
var userController = require('../../../lib/server/controllers/user');

var lab = exports.lab = Lab.script();

lab.experiment('User', function () {

    var user;
    var id;

    var validateUpdated = function (user) {
        Lab.expect(user).to.have.property('id', 'demo-example-com');
        Lab.expect(user).to.have.property('username', 'demo');
        Lab.expect(user).to.have.property('firstname', 'DemoUpdate');
        Lab.expect(user).to.have.property('email', 'demo@example.com');
        Lab.expect(user).to.have.deep.property('roles[0]', 'registered');
        Lab.expect(user).to.have.deep.property('roles[1]', 'manager');
        Lab.expect(user.password).to.equal(undefined);
        Lab.expect(user.key).to.equal(undefined);
    };

    lab.before(function (done) {

        API.setup();
        API.store.start(function (err) {
            if (err) return done(err);

            API.store.models.user.reset(function(err) {
                if (err) return done(err);

                API.store.models.user.all(function(err, results) {
                    if (err) return done(err);

                    results.forEach(function(result) {
                        if (result.id === 'demo-example-com') {
                            user = result.serialize();
                            id = user.id;
                            delete user.id;
                        }
                    });

                    done();
                });

            });
        });
    });

    lab.test('basic', function (done) {

        Lab.expect(userController).to.have.property('setup').that.is.an('function');

        done();
    });

    lab.test('setup', function (done) {

        userController.setup(API.config, API.store);

        done();
    });

    lab.experiment('PUT', function() {

        lab.test('no login update', function (done) {

            var options = {
                method: "PUT",
                url: API.config.base + '/users/' + id,
                payload: { user: user }
            };

            API.server.instance.inject(options, function(response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(401);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('statusCode', 401);
                Lab.expect(result).to.have.property('error', 'Unauthorized');
                Lab.expect(result).to.have.property('message', 'No authorization header');

                done();
            });
        });

        lab.test('logged in unauthorized update', function (done) {

            var options = {
                method: "PUT",
                url: API.config.base + '/users/' + id,
                payload: { user: user },
                credentials: {
                    id: "demo2-example-com",
                    username: "demo2",
                    roles: [ "manager" ]
                }
            };

            API.server.instance.inject(options, function(response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(401);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('statusCode', 401);
                Lab.expect(result).to.have.property('error', 'Unauthorized');
                Lab.expect(result).to.have.property('message', 'Insufficient privileges');

                done();
            });
        });

        lab.test('logged in authorized update', function (done) {
            user.firstname = "DemoUpdate";

            var options = {
                method: "PUT",
                url: API.config.base + '/users/' + id,
                payload: { user: user },
                credentials: {
                    "id": "demo2-example-com",
                    "username": "demo2",
                    roles: [ "manager", "admin" ]
                }
            };

            API.server.instance.inject(options, function(response) {
                 var result = response.result;

                 Lab.expect(response.statusCode).to.equal(200);
                 Lab.expect(result).to.be.a('object');
                 Lab.expect(result).to.have.property('user');
                 validateUpdated(result.user);
                 Lab.expect(user).to.have.property('lastname', 'User');

                 done();
            });
        });

        lab.test('logged in self update', function (done) {
            user.lastname = "Edited";

            var options = {
                method: "PUT",
                url: API.config.base + '/users/' + id,
                payload: { user: user },
                credentials: {
                    "id": "demo-example-com",
                    "username": "demo",
                    roles: [  ]
                }
            };

            API.server.instance.inject(options, function(response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(200);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('user');
                validateUpdated(result.user);
                Lab.expect(user).to.have.property('lastname', 'Edited');

                done();
            });
        });
    });

    lab.experiment('GET', function() {

        lab.test('no login get', function (done) {

            var options = {
                method: "GET",
                url: API.config.base + '/users/' + id
            };

            API.server.instance.inject(options, function(response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(401);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('statusCode', 401);
                Lab.expect(result).to.have.property('error', 'Unauthorized');
                Lab.expect(result).to.have.property('message', 'No authorization header');

                done();
            });
        });

        lab.test('logged in unauthorized get', function (done) {

            var options = {
                method: "GET",
                url: API.config.base + '/users/' + id,
                credentials: {
                    "id": "demo2-example-com",
                    "username": "demo2",
                    roles: [ "manager" ]
                }
            };

            API.server.instance.inject(options, function(response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(401);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('statusCode', 401);
                Lab.expect(result).to.have.property('error', 'Unauthorized');
                Lab.expect(result).to.have.property('message', 'Insufficient privileges');

                done();
            });
        });

        lab.test('logged in authorized get', function (done) {

            var options = {
                method: "GET",
                url: API.config.base + '/users/' + id,
                credentials: {
                    "id": "demo2-example-com",
                    "username": "demo2",
                    roles: [ "manager", "admin" ]
                }
            };

            API.server.instance.inject(options, function(response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(200);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('user');
                validateUpdated(result.user);
                Lab.expect(user).to.have.property('lastname', 'Edited');

                done();
            });
        });

        lab.test('logged in self get', function (done) {

            var options = {
                method: "GET",
                url: API.config.base + '/users/' + id,
                credentials: {
                    "id": "demo-example-com",
                    "username": "demo",
                    roles: [  ]
                }

            };

            API.server.instance.inject(options, function(response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(200);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('user');
                validateUpdated(result.user);
                Lab.expect(user).to.have.property('lastname', 'Edited');

                done();
            });

        });

        lab.test('logged in get nonexistant', function (done) {

            var options = {
                method: "GET",
                url: API.config.base + '/users/doesnt-exist',
                credentials: {
                    "id": "demo-example-com",
                    "username": "demo",
                    roles: [  ]
                }

            };

            API.server.instance.inject(options, function(response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(404);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('statusCode', 404);
                Lab.expect(result).to.have.property('error', 'Not Found');
                Lab.expect(result).to.have.property('message', 'No user found with the id: doesnt-exist');

                done();

            });

        });
    });

    lab.experiment('DELETE', function() {

        lab.test('no login remove', function (done) {

            var options = {
                method: "DELETE",
                url: API.config.base + '/users/' + id
            };

            API.server.instance.inject(options, function(response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(401);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('statusCode', 401);
                Lab.expect(result).to.have.property('error', 'Unauthorized');
                Lab.expect(result).to.have.property('message', 'No authorization header');

                done();
            });
        });

        lab.test('logged in unauthorized remove', function (done) {

            var options = {
                method: "DELETE",
                url: API.config.base + '/users/' + id,
                credentials: {
                    "id": "demo2-example-com",
                    "username": "demo2",
                    roles: [  ]
                }
            };

            API.server.instance.inject(options, function(response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(401);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('statusCode', 401);
                Lab.expect(result).to.have.property('error', 'Unauthorized');
                Lab.expect(result).to.have.property('message', 'Insufficient privileges');

                done();
            });
        });

        lab.test('logged in authorized remove', function (done) {
            var options = {
                method: "DELETE",
                url: API.config.base + '/users/' + id,
                credentials: {
                    "id": "demo2-example-com",
                    "username": "demo2",
                    roles: [ "admin" ]
                }
            };

            API.server.instance.inject(options, function(response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(200);
                Lab.expect(result).to.equal(true);

                API.store.models.user.all(function(err, users) {
                    if (err) return done(err);

                    helpers.verifyArray(users, 'id', {
                        'demo2-example-com': helpers.validators.users.demo2WithAuth,
                        'demo3-example-com': helpers.validators.users.demo3WithAuth
                    });

                    done();
                });
            });

        });

        lab.test('logged in self remove', function (done) {
            var options = {
                method: "DELETE",
                url: API.config.base + '/users/demo2-example-com',
                credentials: {
                    "id": "demo2-example-com",
                    "username": "demo2",
                    roles: [ ]
                }
            };

            API.server.instance.inject(options, function(response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(200);
                Lab.expect(result).to.equal(true);

                API.store.models.user.all(function(err, users) {
                    if (err) return done(err);

                    helpers.verifyArray(users, 'id', {
                        'demo3-example-com': helpers.validators.users.demo3WithAuth
                    });

                    done();
                });
            });

        });

    });

});
