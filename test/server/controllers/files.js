var Lab = require('lab');
var fs = require('fs');

var API = require('../../../lib');
var filesController = require('../../../lib/server/controllers/files');

var lab = exports.lab = Lab.script();

lab.experiment('Files', function () {

    lab.test('basic', function (done) {

        Lab.expect(filesController).to.have.property('setup').that.is.an('function');

        done();
    });

    lab.test('setup', function (done) {

        filesController.setup(API.config, API.store);

        done();
    });

    lab.test('no login upload', function (done) {

        var options = {
            method: "POST",
            url: API.config.base + '/files'
        };

        API.server.instance.inject(options, function(response) {
            var result = response.result;

            Lab.expect(response.statusCode).to.equal(401);
            Lab.expect(result).to.be.a('object');
            Lab.expect(result).to.have.property('statusCode', 401);
            Lab.expect(result).to.have.property('error', 'Unauthorized');
            Lab.expect(result).to.have.property('message', 'No authorization header');

            done();
        });
    });

    lab.test('logged in upload pdf', function (done) {
        var filename = "89001095.pdf";
        var file = fs.readFileSync("./test/upload/" + filename);

        var options = {
            method: "POST",
            url: API.config.base + '/files?filename=' + filename,
            payload: file,
            credentials: {
                "id": "demo2-example-com",
                "username": "demo2",
                roles: [ ]
            }

        };

        API.server.instance.inject(options, function(response) {
            var result = response.result;

            Lab.expect(response.statusCode).to.equal(200);
            Lab.expect(result).to.be.a('object');
            Lab.expect(result).to.have.property('filename', filename);

            done();
        });
    });

    lab.test('logged in upload jpeg', function (done) {
        var filename = "quincy-mine-no-2-shaft-hoist-house-Shaft_No_2.jpg";
        var file = fs.readFileSync("./test/upload/" + filename);

        var options = {
            method: "POST",
            url: API.config.base + '/files?filename=' + filename,
            payload: file,
            credentials: {
                "id": "demo2-example-com",
                "username": "demo2",
                roles: [ ]
            }
        };

        API.server.instance.inject(options, function(response) {
            var result = response.result;

            Lab.expect(response.statusCode).to.equal(200);
            Lab.expect(result).to.be.a('object');
            Lab.expect(result).to.have.property('filename', filename);

            done();
        });
    });

    lab.test('logged in upload gif', function (done) {
        var filename = "dredge3.gif";
        var file = fs.readFileSync("./test/upload/" + filename);

        var options = {
            method: "POST",
            url: API.config.base + '/files?filename=' + filename,
            payload: file,
            credentials: {
                "id": "demo2-example-com",
                "username": "demo2",
                roles: [ ]
            }
        };

        API.server.instance.inject(options, function(response) {
            var result = response.result;

            Lab.expect(response.statusCode).to.equal(200);
            Lab.expect(result).to.be.a('object');
            Lab.expect(result).to.have.property('filename', filename);

            done();
        });
    });

    lab.after(function(done) {
        fs.readdir('./test/content', function (err, files) {
            if (err) throw err;

            files.forEach(function(file) {
                fs.unlinkSync("./test/content/" + file);
            });
            done();
        });
    });

});