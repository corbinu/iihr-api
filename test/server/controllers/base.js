var Lab = require('lab');

var API = require('../../../lib');
var baseController = require('../../../lib/server/controllers/base');

var lab = exports.lab = Lab.script();

lab.experiment('Base', function () {

    lab.test('basic', function (done) {

        Lab.expect(baseController).to.have.property('setup').that.is.an('function');

        done();
    });

    lab.test('setup', function (done) {

        baseController.setup(API.config, API.store);

        done();
    });

    lab.experiment('GET', function() {

        lab.before(function (done) {

            API.setup();

            done();
        });

        lab.test('no login welcome', function(done) {

            var options = {
                method: "GET",
                url: API.config.base + '/'
            };

            API.server.instance.inject(options, function(response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(200);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('hello', 'Welcome to the ' + API.config.name +  ' API');
                Lab.expect(result).to.have.property('version', 'v0');

                done();
            });
        });

        lab.test('logged in welcome', function(done) {

            var options = {
                method: "GET",
                url: API.config.base + '/',
                credentials: {
                    "username": "demo"
                }
            };

            API.server.instance.inject(options, function(response) {
                var result = response.result;

                Lab.expect(response.statusCode).to.equal(200);
                Lab.expect(result).to.be.a('object');
                Lab.expect(result).to.have.property('hello', 'Welcome demo to the ' + API.config.name +  ' API');
                Lab.expect(result).to.have.property('version', 'v0');

                done();
            });
        });

    });

});