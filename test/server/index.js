var Lab = require('lab');

var API = require('../../lib');
var server = require('../../lib/server');

var lab = exports.lab = Lab.script();

lab.experiment('Server', function () {

    lab.test('setup', function (done) {

        server.setup(API.config, API.store);

        done();
    });

    lab.test('basic', function (done) {

        Lab.expect(server).to.have.property('controllers');
        Lab.expect(server).to.have.property('instance');
        Lab.expect(server).to.have.property('auth');
        Lab.expect(server).to.have.property('setup').that.is.an('function');
        Lab.expect(server).to.have.property('start').that.is.an('function');

        done();
    });

});
