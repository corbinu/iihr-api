var Lab = require('lab');

var API = require('../lib');

var lab = exports.lab = Lab.script();

lab.experiment('API', function () {

    lab.test('basic', function (done) {

        Lab.expect(API).to.have.property('config');
        Lab.expect(API).to.have.property('store');
        Lab.expect(API).to.have.property('server');
        Lab.expect(API).to.have.property('setup').that.is.an('function');
        Lab.expect(API).to.have.property('start').that.is.an('function');

        done();
    });

});