var Lab = require('lab');

module.exports = function(docs, key, expected) {
    var unexpectedDoc = function (doc) {
        throw new Error("Unexpected doc " + doc[key]);
    };

    Lab.expect(docs).to.be.an('array').length(Object.keys(expected).length);

    docs.forEach(function (doc) {
        var verify = unexpectedDoc;
        for (var name in expected) {
            if (expected.hasOwnProperty(name)) {
                if (doc[key] === name) {
                    verify = expected[name];
                    expected[name] = unexpectedDoc;
                }
            }
        }
        verify(doc);
    });
};