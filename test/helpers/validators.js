var Lab = require('lab');

var validators = {};

validators.resources = {
    quincyMine: function(resource) {
        Lab.expect(resource).to.have.property('id', 'quincy-mining-company-historic-district');
        Lab.expect(resource).to.have.property('ref', 1);
        Lab.expect(resource).to.have.property('type', 'Archaeological Heritage (Site)');
        Lab.expect(resource).to.have.property('name', 'Quincy Mining Company Historic District');
        Lab.expect(resource).to.have.property('other_names').to.be.an('array').length(1).to.include.members([ "Quincy Mine" ]);
        Lab.expect(resource).to.have.deep.property('location.address', 'TICCIH Address: 49750 U.S. Highway 41<br>TICCIH Postal: 49930<br>Address: Hancock<br>State: MI<br>');
        Lab.expect(resource).to.have.deep.property('location.description', 'Near Hancock, Michigan');
        Lab.expect(resource).to.have.deep.property('location.geojson.type', 'FeatureCollection');
        Lab.expect(resource).to.have.deep.property('location.geojson.features[0].type', 'Point');
        Lab.expect(resource).to.have.deep.property('location.geojson.features[0].coordinates[0]', 47.136667);
        Lab.expect(resource).to.have.deep.property('location.geojson.features[0].coordinates[1]', -88.575);
        Lab.expect(resource).to.have.property('description', "The Quincy Mine is an extensive set of copper mines located near Hancock, Michigan. The mine was owned by the Quincy Mining Company and operated between 1846 and 1945, although some activities continued through the 1970s. The Quincy Mine was known as &quot;Old Reliable,&quot; as the Quincy Mine Company paid a dividend to investors every year from 1868 through 1920. The Quincy Mining Company Historic District is a U.S. National Historic Landmark District; other Quincy Mine properties nearby, including the Quincy Mining Company Stamp Mills, the Quincy Dredge Number Two, and the Quincy Smelter are also historically significant.");
        Lab.expect(resource).to.have.property('classification', "0. EXTRACTIVE INDUSTRIES (EXTRAC)");
        Lab.expect(resource).to.have.property('dating', "1846");
        Lab.expect(resource).to.have.property('condition', "Good");
        Lab.expect(resource).to.have.deep.property('threats[0].name', 'Vandalism');
        Lab.expect(resource).to.have.deep.property('threats[0].date', '04.02.2012');
        Lab.expect(resource).to.have.deep.property('threats[0].description', '4 sites in the Mining district were graffitied');
        Lab.expect(resource).to.have.property('measurements', "779 Acres");
        Lab.expect(resource).to.have.deep.property('references[0].name', 'Wiki');
        Lab.expect(resource).to.have.deep.property('references[0].href', 'http://en.wikipedia.org/wiki/Quincy_Mine');
        Lab.expect(resource).to.have.deep.property('references[1].name', 'NHL');
        Lab.expect(resource).to.have.deep.property('references[1].href', 'http://pdfhost.focus.nps.gov/docs/NHLS/Text/89001095.pdf');
        Lab.expect(resource).to.have.deep.property('references[2].name', 'TICCIH');
        Lab.expect(resource).to.have.deep.property('references[2].href', 'http://www.quincymine.com/');
        Lab.expect(resource).to.have.deep.property('references[3].name', 'HAER');
        Lab.expect(resource).to.have.deep.property('references[3].href', 'http://hdl.loc.gov/loc.pnp/hhh.mi0086');
        Lab.expect(resource).to.have.property('author', 'demo2-example-com');
        Lab.expect(resource).to.have.property('created').to.be.a('date');
        Lab.expect(resource).to.have.property('updates').to.be.an('array').length(0);
        Lab.expect(resource).to.have.property('elements').to.be.an('array').length(1).to.include.members([ 3 ]);
        Lab.expect(resource).to.have.deep.property('images[0].src', 'quincy-mining-company-historic-district-No_2_Shaft_Rock_House.jpg');
        Lab.expect(resource).to.have.deep.property('images[0].caption', 'No 2 Shaft Rock House');
        Lab.expect(resource).to.have.deep.property('images[1].src', 'quincy-mining-company-historic-district-Supply_Office_Powderhouse.jpg');
        Lab.expect(resource).to.have.deep.property('images[1].caption', 'Supply Office (background) and Powderhouse');
        Lab.expect(resource).to.have.deep.property('images[2].src', 'quincy-mining-company-historic-district-QuincyMineNo2Shafthouse.jpg');
        Lab.expect(resource).to.have.deep.property('images[2].caption', 'The #2 Shafthouse (left) and the Hoist House (right)');
        Lab.expect(resource).to.have.deep.property('documents.[0].title', 'Nomination Form');
        Lab.expect(resource).to.have.deep.property('documents.[0].src', 'quincy-mining-company-historic-district-89001095.pdf');
        Lab.expect(resource).to.have.deep.property('documents.[0].description', 'National Register of Historic Places Inventory - Nomination Form');
    },
    cliffMine: function(resource) {
        Lab.expect(resource).to.have.property('id', 'cliff-mine-archaeological-site');
        Lab.expect(resource).to.have.property('ref', 2);
        Lab.expect(resource).to.have.property('type', 'Landscape');
        Lab.expect(resource).to.have.property('name', 'Cliff Mine Archaeological Site');
        Lab.expect(resource).to.have.property('description', "The Cliff Mine, owned and operated by the Pittsburgh & Boston Mining Company, was the first successful copper mine in the Upper Peninsula of Michigan. Discovered in 1845, the “Cliff Vein” produced over 38 million pounds of refined copper over a 40 year period, and paid dividends to its investors totaling $2.5 million.");
        Lab.expect(resource).to.have.property('classification', "0. EXTRACTIVE INDUSTRIES (EXTRAC)");
        Lab.expect(resource).to.have.property('condition', "Poor");
        Lab.expect(resource).to.have.deep.property('references[0].name', 'Website');
        Lab.expect(resource).to.have.deep.property('references[0].href', 'http://cliffmine.wordpress.com/history-of-cliff');
        Lab.expect(resource).to.have.property('author', 'demo-example-com');
        Lab.expect(resource).to.have.property('created').to.be.a('date');
        Lab.expect(resource).to.have.property('updates').to.be.an('array').length(0);
        Lab.expect(resource).to.have.deep.property('images[0].src', 'cliff-mine-archaeological-site-mtu-neg-02619.jpg');
        Lab.expect(resource).to.have.deep.property('images[0].caption', 'Sketch of the Cliff Mine, 1849. (image courtesy of Michigan Tech Archives)');
    },
    quincyMineHoist: function(resource) {
        Lab.expect(resource).to.have.property('id', 'quincy-mine-no-2-shaft-hoist-house');
        Lab.expect(resource).to.have.property('ref', 3);
        Lab.expect(resource).to.have.property('type', 'Engineering Heritage');
        Lab.expect(resource).to.have.property('name', 'Quincy Mine No 2 Shaft Hoist House');
        Lab.expect(resource).to.have.deep.property('location.address', 'Franklin Township, Houghton County, Michigan');
        Lab.expect(resource).to.have.deep.property('location.description', 'Near Hancock, Michigan');
        Lab.expect(resource).to.have.property('description', "The Quincy Mine No. 2 Shaft Hoist House is an industrial building located north of Hancock, Michigan along US-41 within the Quincy Mining Company Historic District. The Hoist House contains the largest steam hoisting engine in the world, which sits on the largest reinforced concrete engine foundation ever poured. The shaft hoist house was designated a Michigan State Historic Site in 1969 and listed on the National Register of Historic Places in 1970.");
        Lab.expect(resource).to.have.property('classification', "0. EXTRACTIVE INDUSTRIES (EXTRAC)");
        Lab.expect(resource).to.have.property('dating', "1918");
        Lab.expect(resource).to.have.property('condition', "Good");
        Lab.expect(resource).to.have.property('significance', 'Wonderfully preserved and fully functional hoist, unique in the Copper Country.');
        Lab.expect(resource).to.have.deep.property('references[0].name', 'Wiki');
        Lab.expect(resource).to.have.deep.property('references[0].href', 'http://en.wikipedia.org/wiki/Quincy_Mine_No._2_Shaft_Hoist_House');
        Lab.expect(resource).to.have.property('author', 'demo-example-com');
        Lab.expect(resource).to.have.property('created').to.be.a('date');
        Lab.expect(resource).to.have.property('updates').to.be.an('array').length(0);
        Lab.expect(resource).to.have.deep.property('images[0].src', 'quincy-mine-no-2-shaft-hoist-house-No_2_Hoist_House.jpg');
        Lab.expect(resource).to.have.deep.property('images[0].caption', 'No. 2 Shaft Hoist House in 2009, on left. (Old hoist house is on right)');
        Lab.expect(resource).to.have.deep.property('images[1].src', 'quincy-mine-no-2-shaft-hoist-house-Shaft_No_2.jpg');
        Lab.expect(resource).to.have.deep.property('images[1].caption', 'Shaft No. 2 with the old hoist house, c1906');
        Lab.expect(resource).to.have.deep.property('images[2].src', 'quincy-mine-no-2-shaft-hoist-house-Hoist_Power_House.JPG');
        Lab.expect(resource).to.have.deep.property('images[2].caption', 'Quincy Mine No. 2 Hoist House, 2006');
        Lab.expect(resource).to.have.deep.property('images[3].src', 'quincy-mine-no-2-shaft-hoist-house-Quincy_Mine_No_2_Hoist_1978.jpg');
        Lab.expect(resource).to.have.deep.property('images[3].caption', 'Quincy Mine No. 2 Hoist House, 1978');
    },
    quincyDredge: function(resource) {
        Lab.expect(resource).to.have.property('id', 'quincy-dredge-2');
        Lab.expect(resource).to.have.property('ref', 4);
        Lab.expect(resource).to.have.property('type', 'Machinery');
        Lab.expect(resource).to.have.property('name', 'Quincy Dredge #2');
        Lab.expect(resource).to.have.deep.property('location.address', 'M-26 near Torch Lake, Osceola Township');
        Lab.expect(resource).to.have.property('description', "The Quincy Dredge Number Two (previously known as the Calumet and Hecla Dredge Number One) is a dredge currently sunk in shallow water in Torch Lake, across M-26 from the Quincy Mining Company Stamp Mills Historic District and just east of Mason in Osceola Township.[2] It was constructed to reclaim stamping sand from the lake for further processing, and was designated a Michigan State Historic Site in 1978. The dredge was usually laid up during the winter months by anchoring it out in the lake, and it carried onboard pumps to combat the slow leakage of water through its hull. Apparently, at 9:00 AM on Sunday January 15, 1956, the pumps stopped working, and before they could be restarted, the dredge plunged to the bottom, leaving only her roof exposed. Raising the dredge was thought to be uneconomical and only limited salvage was conducted. Gradually, the dredge settled into the lake bottom leaving only the peak of her roof above water.");
        Lab.expect(resource).to.have.property('classification', "0. EXTRACTIVE INDUSTRIES (EXTRAC)");
        Lab.expect(resource).to.have.property('dating', "1914");
        Lab.expect(resource).to.have.property('condition', "Reasonable");
        Lab.expect(resource).to.have.property('significance', 'A preserved example of a technology used in the reclamation phase of the area.');
        Lab.expect(resource).to.have.deep.property('references[0].name', 'Website');
        Lab.expect(resource).to.have.deep.property('references[0].href', 'http://www.ship-wrecks.net/shipwreck/keweenaw/dredge.html');
        Lab.expect(resource).to.have.deep.property('references[1].name', 'Wiki');
        Lab.expect(resource).to.have.deep.property('references[1].href', 'http://en.wikipedia.org/wiki/Quincy_Dredge_Number_Two');
        Lab.expect(resource).to.have.property('author', 'demo-example-com');
        Lab.expect(resource).to.have.property('created').to.be.a('date');
        Lab.expect(resource).to.have.property('updates').to.be.an('array').length(0);
        Lab.expect(resource).to.have.deep.property('images[0].src', 'quincy-dredge-2-Calumet_and_Hecla_Dredge.jpg');
        Lab.expect(resource).to.have.deep.property('images[0].caption', 'Dredge half sunk in Torch Lake');
        Lab.expect(resource).to.have.deep.property('images[1].src', 'quincy-dredge-2-dredge3.gif');
        Lab.expect(resource).to.have.deep.property('images[1].caption', 'Map of the dredge location');
    }
};

validators.users = {
    demo: function(user) {
        Lab.expect(user).to.have.property('id', 'demo-example-com');
        Lab.expect(user).to.have.property('username', 'demo');
        Lab.expect(user).to.have.property('firstname', "Demo");
        Lab.expect(user).to.have.property('lastname', "User");
        Lab.expect(user).to.have.property('email', 'demo@example.com');
        Lab.expect(user).to.have.deep.property('roles[0]', 'registered');
        Lab.expect(user).to.have.deep.property('roles[1]', 'manager');
        Lab.expect(user.password).to.equal(undefined);
        Lab.expect(user.key).to.equal(undefined);
    },
    demoWithKey: function(user) {
        Lab.expect(user).to.have.property('id', 'demo-example-com');
        Lab.expect(user).to.have.property('username', 'demo');
        Lab.expect(user).to.have.property('firstname', "Demo");
        Lab.expect(user).to.have.property('lastname', "User");
        Lab.expect(user).to.have.property('email', 'demo@example.com');
        Lab.expect(user).to.have.deep.property('roles[0]', 'registered');
        Lab.expect(user).to.have.deep.property('roles[1]', 'manager');
        Lab.expect(user.password).to.equal(undefined);
        Lab.expect(user.key).to.be.a('string').length(128);
    },
    demo2: function(user) {
        Lab.expect(user).to.have.property('id', 'demo2-example-com');
        Lab.expect(user).to.have.property('username', 'demo2');
        Lab.expect(user).to.have.property('firstname', "Demo2");
        Lab.expect(user).to.have.property('lastname', "User2");
        Lab.expect(user).to.have.property('email', 'demo2@example.com');
        Lab.expect(user).to.have.deep.property('roles[0]', 'registered');
        Lab.expect(user).to.have.deep.property('roles[1]', 'manager');
        Lab.expect(user.password).to.equal(undefined);
        Lab.expect(user.key).to.equal(undefined);
    },
    demo2WithAuth: function(user) {
        Lab.expect(user).to.have.property('id', 'demo2-example-com');
        Lab.expect(user).to.have.property('username', 'demo2');
        Lab.expect(user).to.have.property('firstname', "Demo2");
        Lab.expect(user).to.have.property('lastname', "User2");
        Lab.expect(user).to.have.property('email', 'demo2@example.com');
        Lab.expect(user).to.have.deep.property('roles[0]', 'registered');
        Lab.expect(user).to.have.deep.property('roles[1]', 'manager');
        Lab.expect(user.password).to.be.a('string').length(60);
        Lab.expect(user.key).to.be.a('string').length(128);
    },
    demo3: function(user) {
        Lab.expect(user).to.have.property('id', 'demo3-example-com');
        Lab.expect(user).to.have.property('username', 'demo3');
        Lab.expect(user).to.have.property('firstname', "Demo3");
        Lab.expect(user).to.have.property('lastname', "User3");
        Lab.expect(user).to.have.property('email', 'demo3@example.com');
        Lab.expect(user).to.have.deep.property('roles[0]', 'registered');
        Lab.expect(user).to.have.deep.property('roles[1]', 'manager');
        Lab.expect(user.password).to.equal(undefined);
        Lab.expect(user.key).to.equal(undefined);
    },
    demo3WithAuth: function(user) {
        Lab.expect(user).to.have.property('id', 'demo3-example-com');
        Lab.expect(user).to.have.property('username', 'demo3');
        Lab.expect(user).to.have.property('firstname', "Demo3");
        Lab.expect(user).to.have.property('lastname', "User3");
        Lab.expect(user).to.have.property('email', 'demo3@example.com');
        Lab.expect(user).to.have.deep.property('roles[0]', 'registered');
        Lab.expect(user).to.have.deep.property('roles[1]', 'manager');
        Lab.expect(user.password).to.be.a('string').length(60);
        Lab.expect(user.key).to.be.a('string').length(128);
    },
};

module.exports = validators;