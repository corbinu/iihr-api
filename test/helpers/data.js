var data = {};

data.resources = [
    {
        "type": "Archaeological Heritage (Site)",
        "name": "Quincy Mining Company Historic District",
        "other_names": [ "Quincy Mine" ],
        "description": "The Quincy Mine is an extensive set of copper mines located near Hancock, Michigan. The mine was owned by the Quincy Mining Company and operated between 1846 and 1945, although some activities continued through the 1970s. The Quincy Mine was known as &quot;Old Reliable,&quot; as the Quincy Mine Company paid a dividend to investors every year from 1868 through 1920. The Quincy Mining Company Historic District is a U.S. National Historic Landmark District; other Quincy Mine properties nearby, including the Quincy Mining Company Stamp Mills, the Quincy Dredge Number Two, and the Quincy Smelter are also historically significant.",
        "classification": "0. EXTRACTIVE INDUSTRIES (EXTRAC)",
        "dating": "1846",
        "condition": "Good",
        "images": [
            {
                "src": "quincy-mining-company-historic-district-No_2_Shaft_Rock_House.jpg",
                "caption": "No 2 Shaft Rock House"
            },
            {
                "src": "quincy-mining-company-historic-district-Supply_Office_Powderhouse.jpg",
                "caption": "Supply Office (background) and Powderhouse"
            },
            {
                "src": "quincy-mining-company-historic-district-QuincyMineNo2Shafthouse.jpg",
                "caption": "The #2 Shafthouse (left) and the Hoist House (right)"
            }
        ],
        "measurements": "779 Acres",
        "threats": [
            {
                "name": "Vandalism",
                "date": "04.02.2012",
                "description": "4 sites in the Mining district were graffitied"
            }
        ],
        "references": [
            {
                "name": "Wiki",
                "href": "http://en.wikipedia.org/wiki/Quincy_Mine"
            },
            {
                "name": "NHL",
                "href": "http://pdfhost.focus.nps.gov/docs/NHLS/Text/89001095.pdf"
            },
            {
                "name": "TICCIH",
                "href": "http://www.quincymine.com/"
            },
            {
                "name": "HAER",
                "href": "http://hdl.loc.gov/loc.pnp/hhh.mi0086"
            }
        ],
        "elements": [ 3 ],
        "documents": [
            {
                "title": "Nomination Form",
                "src": "quincy-mining-company-historic-district-89001095.pdf",
                "description": "National Register of Historic Places Inventory - Nomination Form"
            }
        ],
        "author": "demo2-example-com",
        "location": {
            "address": "TICCIH Address: 49750 U.S. Highway 41<br>TICCIH Postal: 49930<br>Address: Hancock<br>State: MI<br>",
            "description": "Near Hancock, Michigan",
            "geojson": {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Point",
                        "coordinates": [
                            47.136667,
                            -88.575
                        ]
                    }
                ]
            }
        }
    },
    {
        "type": "Landscape",
        "name": "Cliff Mine Archaeological Site",
        "description": "The Cliff Mine, owned and operated by the Pittsburgh & Boston Mining Company, was the first successful copper mine in the Upper Peninsula of Michigan. Discovered in 1845, the “Cliff Vein” produced over 38 million pounds of refined copper over a 40 year period, and paid dividends to its investors totaling $2.5 million.",
        "classification": "0. EXTRACTIVE INDUSTRIES (EXTRAC)",
        "condition": "Poor",
        "author": "demo2-example-com",
        "images": [
            {
                "src": "cliff-mine-archaeological-site-mtu-neg-02619.jpg",
                "caption": "Sketch of the Cliff Mine, 1849. (image courtesy of Michigan Tech Archives)"
            }
        ],
        "references": [
            {
                "name": "Website",
                "href": "http://cliffmine.wordpress.com/history-of-cliff"
            }
        ]
    },
    {
        "type": "Engineering Heritage",
        "name": "Quincy Mine No 2 Shaft Hoist House",
        "dating": "1918",
        "condition": "Good",
        "significance": "Wonderfully preserved and fully functional hoist, unique in the Copper Country.",
        "description": "The Quincy Mine No. 2 Shaft Hoist House is an industrial building located north of Hancock, Michigan along US-41 within the Quincy Mining Company Historic District. The Hoist House contains the largest steam hoisting engine in the world, which sits on the largest reinforced concrete engine foundation ever poured. The shaft hoist house was designated a Michigan State Historic Site in 1969 and listed on the National Register of Historic Places in 1970.",
        "classification": "0. EXTRACTIVE INDUSTRIES (EXTRAC)",
        "author": "demo-example-com",
        "images": [
            {
                "src": "quincy-mine-no-2-shaft-hoist-house-No_2_Hoist_House.jpg",
                "caption": "No. 2 Shaft Hoist House in 2009, on left. (Old hoist house is on right)"
            },
            {
                "src": "quincy-mine-no-2-shaft-hoist-house-Shaft_No_2.jpg",
                "caption": "Shaft No. 2 with the old hoist house, c1906"
            },
            {
                "src": "quincy-mine-no-2-shaft-hoist-house-Hoist_Power_House.JPG",
                "caption": "Quincy Mine No. 2 Hoist House, 2006"
            },
            {
                "src": "quincy-mine-no-2-shaft-hoist-house-Quincy_Mine_No_2_Hoist_1978.jpg",
                "caption": "Quincy Mine No. 2 Hoist House, 1978"
            }
        ],
        "references": [
            {
                "name": "Wiki",
                "href": "http://en.wikipedia.org/wiki/Quincy_Mine_No._2_Shaft_Hoist_House"
            }
        ],
        "location": {
            "address": "Franklin Township, Houghton County, Michigan",
            "description": "Near Hancock, Michigan"
        }
    },
    {
        "type": "Machinery",
        "name": "Quincy Dredge #2",
        "dating": "1914",
        "description": "The Quincy Dredge Number Two (previously known as the Calumet and Hecla Dredge Number One) is a dredge currently sunk in shallow water in Torch Lake, across M-26 from the Quincy Mining Company Stamp Mills Historic District and just east of Mason in Osceola Township.[2] It was constructed to reclaim stamping sand from the lake for further processing, and was designated a Michigan State Historic Site in 1978. The dredge was usually laid up during the winter months by anchoring it out in the lake, and it carried onboard pumps to combat the slow leakage of water through its hull. Apparently, at 9:00 AM on Sunday January 15, 1956, the pumps stopped working, and before they could be restarted, the dredge plunged to the bottom, leaving only her roof exposed. Raising the dredge was thought to be uneconomical and only limited salvage was conducted. Gradually, the dredge settled into the lake bottom leaving only the peak of her roof above water.",
        "classification": "0. EXTRACTIVE INDUSTRIES (EXTRAC)",
        "significance": "A preserved example of a technology used in the reclamation phase of the area.",
        "author": "demo3-example-com",
        "condition": "Reasonable",
        "images": [
            {
                "src": "quincy-dredge-2-Calumet_and_Hecla_Dredge.jpg",
                "caption": "Dredge half sunk in Torch Lake"
            },
            {
                "src": "quincy-dredge-2-dredge3.gif",
                "caption": "Map of the dredge location"
            }
        ],
        "location": {
            "address": "M-26 near Torch Lake, Osceola Township"
        },
        "references": [
            {
                "name": "Website",
                "href": "http://www.ship-wrecks.net/shipwreck/keweenaw/dredge.html"
            },
            {
                "name": "Wiki",
                "href": "http://en.wikipedia.org/wiki/Quincy_Dredge_Number_Two"
            }
        ]
    }
];

data.users = [
    {
        "username": "demo",
        "firstname": "Demo",
        "lastname": "User",
        "email": "demo@example.com",
        "password": "password",
        "roles": [
            "registered",
            "manager"
        ]
    },
    {
        "username": "demo2",
        "firstname": "Demo2",
        "lastname": "User2",
        "email": "demo2@example.com",
        "password": "password2",
        "roles": [
            "registered",
            "manager"
        ]
    },
    {
        "username": "demo3",
        "firstname": "Demo3",
        "lastname": "User3",
        "email": "demo3@example.com",
        "password": "password3",
        "roles": [
            "registered",
            "manager"
        ]
    }
];

module.exports = data;