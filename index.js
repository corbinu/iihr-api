var iihr = require('./lib');

iihr.setup();

iihr.start(function (info) {
	console.log('IIHR API started on port: ', info.port);
});