Inventory of Industrial Heritage Resources
==========================

[ ![Codeship Status for corbinu/iihr-api](https://www.codeship.io/projects/b9e56d10-f022-0131-6150-66d8ca7dad4d/status)](https://www.codeship.io/projects/27324)
[ ![Dependencies up to date](https://david-dm.org/inventoryihr/iihr-api.svg)](https://david-dm.org/inventoryihr/iihr-api)

Server for REST API

Requires
------------
Node.js
Elasticsearch
Couchbase

Install
------------
Dev/Test: npm install
Production: npm install --production

Test
------------
npm test

Start
-----------
Dev: npm start
Test: NODE_ENV=test npm start
Production: NODE_ENV=production start
PM2: pm2 start iihr-api.json